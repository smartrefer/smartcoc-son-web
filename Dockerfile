FROM keymetrics/pm2:16-alpine

LABEL maintainer="Thawatchai Seangduan <thawatchai.sea2@gmail.com>"

WORKDIR /home/app

RUN apk update && apk upgrade && apk add --no-cache alpine-sdk git \
  alpine-sdk \
  openssh \
  autoconf \
  tzdata \
  build-base \
  libtool \
  bash \
  automake \
  g++ \
  go \
  ruby \
  make \
  && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
  && echo "Asia/Bangkok" > /etc/timezone

COPY . .

RUN npm i --force && npm run build

RUN rm -rf src

CMD ["pm2-runtime", "process.json"]
