export interface EvaluateData {
    evaluate_id: number;
    coc_home_id: number;
    question_type_id: number;
    question_id: number;
    answer_id?: number;
    answer_text?: string;
    answer_score?: number;
    record_by?: string;
}