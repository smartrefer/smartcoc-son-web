import { Component, ViewEncapsulation } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { RegisterService } from '../../../services/register.service';
import { HomevisitService } from '../../../services/homevisit.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SweetAlertService } from '../../../shared/sweetalert.service';
import { BarthelDialog } from '../sendhomevisit_view/barthel/barthel.component';
import { AdlDialog } from '../sendhomevisit_view/adl/adl.component';
import { EvaluateNtritionDialog } from '../sendhomevisit_view/evaluate-nutrition/evaluate-nutrition.component';
import { ChildDevelopDialog } from '../sendhomevisit_view/child-develop/child-develop.component';
import { ChildVisitDialog } from '../sendhomevisit_view/child-visit/child-visit.component';
import { CvdRiskDialog } from '../sendhomevisit_view/cvd-risk/cvd-risk.component';
import { ElderlyDialog } from '../sendhomevisit_view/elderly/elderly.component';
import { EpidemDialog } from '../sendhomevisit_view/epidem/epidem.component';
import { EsasDialog } from '../sendhomevisit_view/esas/esas.component';
import { ModifiedRankingDialog } from '../sendhomevisit_view/modified-ranking/modified-ranking.component';
import { MotorPowerDialog } from '../sendhomevisit_view/motor-power/motor-power.component';
import { PainKillerDialog } from '../sendhomevisit_view/pain-killer/pain-killer.component';
import { PostnatalDialog } from '../sendhomevisit_view/postnatal/postnatal.component';
import { PpsDialog } from '../sendhomevisit_view/pps/pps.component';
import { PsycheDialog } from '../sendhomevisit_view/psyche/psyche.component';
import { ScreenNutritionDialog } from '../sendhomevisit_view/screen-nutrition/screen-nutrition.component';
import { ViolentlyDialog } from '../sendhomevisit_view/violently/violently.component';
import { AdvanceCarePlanDialog } from '../sendhomevisit_view/advance-care-plan/advance-care-plan.component';
import { OtherDialog } from '../sendhomevisit_view/other/other.component';

import { DateAdapter } from '@angular/material/core';

@Component({
    selector: 'landing-home',
    templateUrl: './result.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ResultComponent {
    itemStorage: any = [];

    problem_and_comment : any ='';
    validateForm: boolean = false;
    panelOpenState = false;
    extra_detail: any;
    diag_text: any;
    profile: any;
    result_code: any = '';
    plan_code: any = '';
    plan_name: any = '';
    summary: any = '';
    question: any;
    result: any = [];
    temp_data: any = [];
    address_lat: any;
    address_long: any;

    /**
     * Constructor
     */
    constructor(
        private router: Router,
        private registerService: RegisterService,
        private homevisitService: HomevisitService,
        private spinner: NgxSpinnerService,
        private sweetAlertService: SweetAlertService,
        public dialog: MatDialog,
        private dateAdapter: DateAdapter<Date>) 
    {
        this.dateAdapter.setLocale('th-TH');
        let username = sessionStorage.getItem('username');
        if(!username){
            this.router.navigate(['/sign-in']);
        }
    }

    ngOnInit() {
        this.spinner.show();
        let i: any = sessionStorage.getItem('itemRegister');
        this.itemStorage = JSON.parse(i);

        // console.log(this.itemStorage);

        this.getHomevisit();

        // this.getResult();
        this.spinner.hide();
    }

    async getHomevisit() {
        let info: any = {
            "coc_home_id": this.itemStorage.coc_home_id
        }

        try {
            // รับข้อมูล ลงเยี่ยมบ้าน
            let rs: any = await this.homevisitService.getHomeVisitOne(info);
            // console.log(rs);
            this.profile = rs[0];
            this.extra_detail = this.profile.extra_detail;
            if(!this.extra_detail){
                this.extra_detail = '';
            }
            this.result_code = this.profile.result_code;
            this.plan_code = this.profile.plan_code;
            this.plan_name = this.profile.plan_name;
            this.summary = this.profile.summary;
            this.problem_and_comment = this.profile.problem_and_comment;
            // รับข้อมูล คำถาม
            try {
                this.question = await this.homevisitService.getQuestion();
                this.question = this.question.splice(3, 24);
                // console.log(this.question);
            } catch (error) {
                console.log(error)
            }

            // รับข้อมูล การประเมิน
            this.result = await this.getResult(this.itemStorage.coc_home_id);

            console.log(this.profile);
        } catch (error) {
            console.log(error);
        }

    }

    async getEvaluate(id: any, question_type_id: any) {

        let info: any = {
            "coc_home_id": id,
            "question_type_id": question_type_id
        }

        try {
            let rs: any = await this.homevisitService.getEvaluate(info);
            // console.log(rs);
            return rs;
        } catch (error) {
            console.log(error);
        }

    }

    async getEvaluateScore(id: any, question_type_id: any) {
        let info: any = {
            "coc_home_id": id,
            "question_type_id": question_type_id
        }
        try {
            let rs: any = await this.homevisitService.getEvaluateScore(info);
            // console.log(rs);
            return rs;
        } catch (error) {
            console.log(error);
        }
    }
    async getResult(id: any) {
        let result: any = [];

        this.question.forEach(async element => {
            try {
                let e: any = await this.getEvaluate(id, element.question_type_id);
                let c = await this.getEvaluateScore(id, element.question_type_id);
                //console.log(e);
                let data: any = {
                    question_type_id: element.question_type_id,
                    question_text: element.questtion_type_name,
                    evaluate_data: e,
                    evaluate_score: c[0]
                }
                if (e.length > 0) {
                    result.push(data);
                }
            }
            catch (error) {
                console.log(error);
            }
        });
        // console.log(result);
        return result;
    }

    async openDialog(question: any) {
        if (question.question_type_id == 4) {
            this.openDialogPsyche(question);
        }
        if (question.question_type_id == 5) {
            this.openDialogViolent(question);
        }
        if (question.question_type_id == 6) {
            this.openDialogBarthel(question);
        }
        if (question.question_type_id == 7) {
            this.openDialogPps(question);
        }
        if (question.question_type_id == 8) {
            this.openDialogEsas(question);
        }
        if (question.question_type_id == 9) {
            this.openDialogPain(question);
        }
        if (question.question_type_id == 10) {
            this.openDialogAdl(question);
        }
        if (question.question_type_id == 11) {
            this.openDialogScreen(question);
        }
        if (question.question_type_id == 12) {
            this.openDialogEvaluateNutrition(question);
        }
        if (question.question_type_id == 13) {
            this.openDialogModified(question);
        }
        if (question.question_type_id == 14) {
            this.openDialogMotor(question);
        }
        if (question.question_type_id == 15) {
            this.openDialogPostnatal(question);
        }
        if (question.question_type_id == 16) {
            this.openDialogChildVisit(question);
        }
        if (question.question_type_id == 17) {
            this.openDialogChildDev(question);
        }
        if (question.question_type_id == 18) {
            this.openDialogEpidem(question);
        }
        if (question.question_type_id == 19) {
            this.openDialogElderly(question);
        }
        if (question.question_type_id == 20) {
            this.openDialogAdvance(question);
        }
        if (question.question_type_id == 21) {
            this.openDialogOther(question);
        }
        if (question.question_type_id == 22) {
            this.openDialogChronic(question);
        }
    }

    async openDialogPsyche(data) {
        const dialogRef = this.dialog.open(PsycheDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogViolent(data) {
        const dialogRef = this.dialog.open(ViolentlyDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogBarthel(data) {
        const dialogRef = this.dialog.open(BarthelDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogPps(data) {
        const dialogRef = this.dialog.open(PpsDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogEsas(data) {
        const dialogRef = this.dialog.open(EsasDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogPain(data) {
        const dialogRef = this.dialog.open(PainKillerDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogAdl(data) {
        const dialogRef = this.dialog.open(AdlDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogScreen(data) {
        const dialogRef = this.dialog.open(ScreenNutritionDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogEvaluateNutrition(data) {
        const dialogRef = this.dialog.open(EvaluateNtritionDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogModified(data) {
        const dialogRef = this.dialog.open(ModifiedRankingDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogMotor(data) {
        const dialogRef = this.dialog.open(MotorPowerDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogPostnatal(data) {
        const dialogRef = this.dialog.open(PostnatalDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogChildVisit(data) {
        const dialogRef = this.dialog.open(ChildVisitDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogChildDev(data) {
        const dialogRef = this.dialog.open(ChildDevelopDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogEpidem(data) {
        const dialogRef = this.dialog.open(EpidemDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogElderly(data) {
        const dialogRef = this.dialog.open(ElderlyDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogAdvance(data) {
        const dialogRef = this.dialog.open(AdvanceCarePlanDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogOther(data) {
        const dialogRef = this.dialog.open(OtherDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async openDialogChronic(data) {
        const dialogRef = this.dialog.open(CvdRiskDialog, {
            autoFocus: false,
            width: '600px',
            maxHeight: '90vb',
            data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    async back() {
        let route = sessionStorage.getItem('route');
        this.router.navigate([route]);
    }

    async chkradio(){
        console.log('xx'+this.plan_code);

        if(this.plan_code == '8' || this.plan_code == "9"){
            this.plan_name = null;
        }
    }

    async save(){
        let validate = true;
        if(!this.result_code){
            this.sweetAlertService.error('คำชี้แจง', 'กรุณาเลือกผลการประเมิน', 'SmartRefer Ubon');
            validate = false;
        }
        if(!this.plan_code){
            this.sweetAlertService.error('คำชี้แจง', 'กรุณาเลือกแผนการดูแล', 'SmartRefer Ubon');
            validate = false;
        }
        if(!this.summary){
            this.sweetAlertService.error('คำชี้แจง', 'กรุณากรอกสรุปผลการประเมิน', 'SmartRefer Ubon');
            validate = false;
        }

        let data = {
            summary : this.summary,
            problem_and_comment : this.problem_and_comment,
            result_code : this.result_code,
            plan_code : this.plan_code,
            plan_name : this.plan_name
        };
        console.log(data);
        // console.log(validate);
        if(validate){

            try {
                let res :any = await this.homevisitService.saveHomeVisit(this.profile.coc_home_id,data);
                // console.log(res);
                if(res.ok == false){
                    this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถบันทึกข้อมูลได้', 'SmartRefer Ubon');
                } else {
                    this.sweetAlertService.toastSucces('คำชี้แจง', 'บันทึกข้อมูลเรียบร้อย', 'SmartRefer Ubon');
                }
                this.router.navigate(['/coc']);
            } catch (error) {
                this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถบันทึกข้อมูลได้', 'SmartRefer Ubon');
                }
        }
    }

}
