import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { EditDialog } from './edit/edit.component';

import { SendhomevisitComponent } from 'app/modules/landing/sendhomevisit/sendhomevisit.component';
import { sendhomevisitRoutes } from 'app/modules/landing/sendhomevisit/sendhomevisit.routing';

@NgModule({
  declarations: [
    SendhomevisitComponent,
    EditDialog
  ],
  imports     : [
      RouterModule.forChild(sendhomevisitRoutes),
      SharedModule,
      MatAutocompleteModule
  ]
})
export class SendhomevisitModule {

 }
