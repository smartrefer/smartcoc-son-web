import { Component, ViewChild, ViewEncapsulation, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { AsyncPipe } from '@angular/common';
import { Router } from '@angular/router';
import { RegisterService } from '../../../services/register.service';
import { ThaiaddressService } from '../../../services/thaiaddress.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { SweetAlertService } from '../../../shared/sweetalert.service';
import { DateAdapter } from '@angular/material/core';
import * as moment from 'moment-timezone';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EditDialog } from './edit/edit.component';

export interface PeriodicElementDrug {
    action: any;
    drugdate: string;
    drugname: string;
    druguse: string;
    drugqty: number;
}
interface Diagnosis {
    code: string;
    name: string;
}

@Component({
    selector: 'landing-home',
    templateUrl: './sendhomevisit.component.html',
    encapsulation: ViewEncapsulation.None,
})

export class SendhomevisitComponent {
    itemStorage: any = [];
    itemReferLevel: any = [];
    itemPatientStatus: any = [];
    itemCatagory: any = [];
    refer_no: any;
    sdate: any;
    edate: any;
    validateForm: boolean = false;
    panelOpenState = false;
    refer_level: any;
    patient_status: any;
    refer_cause: any;
    cid: any;
    title: any;
    first_name: any;
    middle_name: any;
    last_name: any;
    gender: any;
    gender_name: any;
    dob: any;
    age: any = '0-0-0';
    house_no: any;
    patient_address_name: any;
    phone_number: any = '';
    diag_text: any = '';
    refer_hcode: any;
    refer_to_hcode: any;
    evaluate_clause: any;
    extra_detail: any = '';
    patient_right: any;
    patient_right_code: any;
    discharge_date: any;
    hn: any;
    itemProfile: any = [];
    itemAllergy: any = [];
    itemMedrecconcile: any = [];
    itemService: any = [];
    dataSourceDrug: any = [];
    dataSourceLab: any = [];
    dataSourceXray: any = [];
    dataSourceDrugAllergy: any = [];
    hcode: string;
    evaluate_clause_check: any = [];
    visit_date: any;

    list_hospital: any = [];
    searchHospital: string = '';
    refer_hospname: string = '';
    refer_to_hospname: string = '';
    is_select_hospcode: boolean = false;

    med_reconcile_checked: any = [];
    lab_checked: any = [];
    xray_checked: any = [];
    allergy_checked: any = [];
    appointment: any = [];
    list_department: any = [];

    displayedColumnsDrug: string[] = [
        'action',
        'drugdate',
        'drugname',
        'druguse',
        'drugqty',
    ];
    displayedColumnsLab: string[] = [
        'action',
        'labdate',
        'labname',
        'labresult',
        'labnormal',
    ];
    displayedColumnsXray: string[] = [
        'action',
        'xraydate',
        'xrayname',
        'xrayresult',
        'interpret',
        'comments',
    ];
    displayedColumnsDrugAllergy: string[] = [
        'action',
        'drugallergydate',
        'drugallergyname',
        'drugallergysymptom',
        'drugallergylevel',
        'drugallergyfind',
    ];

    listItem: any = [];

    items: any = [];

    formControl = new FormControl<string>('');

    list_all_hospital: any = [];
    filteredHospital: any[] = [];

    listIcd10Who: any;
    icd10: any;

    selectedDiagnosisCode: any = '';
    selectedDiagnosisType: any = '';

    diagnosis: any = [];
    listDiagType = [
        { id: 1, name: 'Principle' },
        { id: 2, name: 'Comorbidity' },
        { id: 3, name: 'Complication' },
        { id: 4, name: 'Other' },
        { id: 5, name: 'External cause' },
    ];

    filteredDiagnosis: Observable<Diagnosis[]>;

    villageCode: any = '';
    villno: any = '';
    villageName: any = '';
    is_smoke: any = '';

    department: any = '';
    filteredDepartment: any = [];

    searchDepartment: any = '';

    filterIcd10: any = [];
    is_reload_icd10: boolean = false;

    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(
        private router: Router,
        private registerService: RegisterService,
        private thaiaddressService: ThaiaddressService,
        public dialog: MatDialog,
        private spinner: NgxSpinnerService,
        private sweetAlertService: SweetAlertService,
        private dateAdapter: DateAdapter<Date>) {
        this.dateAdapter.setLocale('th-TH');
        this.hcode = sessionStorage.getItem('hcode');
        if (!this.hcode) {
            this.hcode = localStorage.getItem('hcode');
        }

        let username = sessionStorage.getItem('username');
        if (!username) {
            this.router.navigate(['/sign-in']);
        }
        this.list_all_hospital = localStorage.getItem('list_all_hospital');
        this.list_all_hospital = JSON.parse(this.list_all_hospital);
        // console.log('length:', this.list_all_hospital.length);

        this.listIcd10Who = localStorage.getItem('list_icd10');
        this.listIcd10Who = JSON.parse(this.listIcd10Who);
        let icd10 = sessionStorage.getItem('list_icd10');

        if (!this.listIcd10Who) {
            if(icd10 == null || icd10 == undefined || icd10 == ''){
                this.is_reload_icd10 = true;
            } else {
                this.listIcd10Who = JSON.parse(icd10);
                this.is_reload_icd10 = false;
            }
        } else {
            // console.log('icd10:', this.listIcd10Who);
            this.is_reload_icd10 = false;
        }

        let i: any = sessionStorage.getItem('itemStorage');
        this.itemStorage = JSON.parse(i);

        if (this.itemStorage.refer_no) {
            this.refer_no = this.itemStorage.refer_no;
        } else {
            this.refer_no = '';
        }

        let appoint_date = new Date();
        // add appoint_date + 3 days from now and if select day in sat or sun add 2 day
        appoint_date.setDate(appoint_date.getDate() + 3);
        if (appoint_date.getDay() == 6) {
            appoint_date.setDate(appoint_date.getDate() + 2);
        } else if (appoint_date.getDay() == 0) {
            appoint_date.setDate(appoint_date.getDate() + 1);
        }
        // set appoint_date iso string
        this.visit_date = appoint_date;
    }

    // inital data
    async ngOnInit() {
        this.spinner.show();
        await this.list_catagory();
        await this.list_items();
        await this.list_refer_level();
        await this.list_patient_status();
        await this.getDepartment();
        await this.getHospital();
        await this.load_data();
        if(this.is_reload_icd10){
            await this.getIcd10();
        }
        // console.log('house no:',this.house_no,' villno:', this.villno, ' villname:', this.villageName);
        this.spinner.hide();
    }

    async getVillageName(province: any, district: any, subdistrict: any, village: any) {
        await this.thaiaddressService.getVillage(province, district, subdistrict).then((res: any) => {
            let v = res.find((x: any) => x.code == village);
            this.villageName = v.name;
        }).catch((err: any) => {
            console.log(err);
        });
    }

    // load patient data
    async load_data() {
        const hn = sessionStorage.getItem('hn');
        let rs: any = await this.registerService.select_coc_patient(hn);
        let address_his: any;
        // console.log("profile",rs);
        
        
        // console.log("this.itemStorage",this.itemStorage);
        await this.getVillageName(rs.profile[0].chwpart, rs.profile[0].amppart, rs.profile[0].tmbpart, rs.profile[0].moopart);
        let rs_address: any = await this.thaiaddressService.select_full(
            rs.profile[0].chwpart,
            rs.profile[0].amppart,
            rs.profile[0].tmbpart,
            rs.profile[0].moopart
        );
        if (rs_address.length > 0) {
            address_his = rs_address[0].full_name;
        } else {
            address_his = 'ไม่พบข้อมูล';
        }
        this.house_no = rs.profile[0].addrpart;
        this.villno = Number(rs.profile[0].moopart).toString();
        this.villageCode = rs.profile[0].chwpart + rs.profile[0].amppart + rs.profile[0].tmbpart + rs.profile[0].moopart;
        this.itemProfile = rs.profile[0];
        this.itemAllergy = rs.allergy;
        // this.itemMedrecconcile = rs.medrecconcile[0];
        this.itemService = rs.service[0];

        this.dataSourceDrug = this.itemService.drugs;
        this.dataSourceLab = this.itemService.lab;
        this.dataSourceXray = this.itemService.xray;
        this.dataSourceDrugAllergy = this.itemAllergy;

        this.cid = this.itemProfile.cid;
        this.title = this.itemProfile.title_name;
        this.first_name = this.itemProfile.first_name;
        
        this.middle_name = '';
        this.last_name = this.itemProfile.last_name;
        this.gender = '';
        this.gender_name = this.itemProfile.sex;
        this.dob = this.itemProfile.brthdate;
        this.age = this.itemProfile.age;
        this.patient_address_name = this.itemProfile.addrpart + ' ' + address_his;
        this.phone_number = this.itemProfile.contact_mobile;
        this.diag_text = this.itemProfile.diag_text;
        this.refer_hcode = this.itemStorage.refer_hospcode;
        this.refer_to_hcode = this.itemStorage.refer_to_hospcode;
        this.evaluate_clause = this.evaluate_clause;
        this.patient_right = this.itemStorage.patient_right;
        this.patient_right_code = this.itemStorage.patient_type_code;
        this.discharge_date = this.itemStorage.discharge_date;

        if (this.refer_hcode == '' || this.refer_hcode == null) {
            this.refer_hcode = this.hcode;
        }

        if (this.refer_to_hcode == '' || this.refer_to_hcode == null) {
            this.refer_to_hcode = this.hcode;
        }

        // set refer hospital name
        try {
            let rs_hosp: any = await this.thaiaddressService.select_hospcode(
                this.refer_hcode
            );
            // console.log(rs_hosp);
            this.refer_hospname = rs_hosp[0].hosname;
        } catch (error) {
            console.log(error);
        }

        // set refer to hospital name
        try {
            let rs_hosp: any = await this.thaiaddressService.select_hospcode(
                this.refer_to_hcode
            );
            // console.log(rs_hosp);
            this.refer_to_hospname = rs_hosp[0].hosname;
        } catch (error) {
            console.log(error);
        }
    }

    // get all hospital
    async getAllHospital() {
        try {
            await this.thaiaddressService.select_all().then((res: any) => {
                this.list_all_hospital = res;
            });
        } catch (error) {
            console.log(error);
        }
        // console.log(this.list_all_hospital);
    }

    // get hospital from hcode
    async getHospital() {
        try {
            await this.thaiaddressService.select_same_district(
                this.hcode
            ).then((rs: any) => {
                this.list_hospital = rs;
                this.filteredHospital = this.list_hospital;
            });

        } catch (error) {
            console.log(error);
        }
        //    console.log(this.filteredHospital)
    }

    // get department
    async getDepartment() {
        try {
            await this.registerService.department().then((res: any) => {
                this.list_department = res;
                this.filteredDepartment = this.list_department;
            });
        } catch (error) {
            console.log(error);
        }
        // console.log(this.list_department);
    }

    // get refer level
    async list_refer_level() {
        try {
            await this.registerService.list_refer_level().then((res: any) => {
                this.itemReferLevel = res;
            });
        } catch (error) {
            console.log(error);
        }
    }

    // get patient status
    async list_patient_status() {
        try {
            await this.registerService.list_patient_status().then((res: any) => {
                this.itemPatientStatus = res;
            });
        } catch (error) {
            console.log(error);
        }
    }

    // get catagory
    async list_catagory() {
        try {
            await this.registerService.list_catagory().then((res: any) => {
                this.itemCatagory = res;
                this.itemCatagory.forEach((element: any) => {
                    element.ngModel = false;
                });
            });
        } catch (error) {
            console.log(error);
        }
        // console.log('catagory', this.itemCatagory);
    }

    // get items
    async list_items() {
        try {
            await this.registerService.list_items().then((res: any) => {
                this.listItem = res;
                this.listItem.forEach((element: any) => {
                    element.ngModel = false;
                });
            });
        } catch (error) {
            console.log(error);
        }
        // console.log('items', this.listItem);
    }

    // get Icd10
    async getIcd10() {
        try {
            await this.thaiaddressService.getIcd10().then((res: any) => {
                this.listIcd10Who = res;
                sessionStorage.setItem('list_icd10', JSON.stringify(this.listIcd10Who));
                // console.log('icd10:', this.listIcd10Who);
            });
        } catch (error) {
            console.log(error);
        }
    }

    async back() {
        this.router.navigate(['/register']);
    }

    // remove item from array
    removeItemArray(array, item) {
        for (var i in array) {
            if (array[i] == item) {
                array.splice(i, 1);
                break;
            }
        }
    }

    // action when checkbox item catagory change
    onCheckboxChange(e) {
        var ev = e;
        if (ev.checked) {
            this.evaluate_clause_check.push(Number(ev.source.value));
        } else {
            this.removeItemArray(
                this.evaluate_clause_check,
                Number(ev.source.value)
            );
        }
        this.evaluate_clause_check.map(Number);
        this.evaluate_clause_check.sort();
    }

    // action when checkbox list items change
    home_visit(e: any) {
        e
        if (e.checked) {
            this.items.push(e.source.value);
        } else {
            this.removeItemArray(this.items, e.source.value);
        }
        this.items.sort();
        // console.log(this.items);
    }

    // save register coc to server smart refer
    async save_coc_register() {
        // console.log("save_coc_register");
        let coc_appointment: any = {};
        let equipments: any = [];
        for (let i of this.items) {
            let item = this.listItem.find((x: any) => x.id == i);
            let data = {
                id: i,
                name: item.name
            };
            equipments.push(data);
        }
        if (this.evaluate_clause_check.length == 0) {
            alert('กรุณาเลือกสาเหตุการเยี่ยมบ้าน')
            return;
        }
        const diag = this.diagnosis.find((x: any) => x.diag_type == 1);
        let diagnosis_text = '';
        if(diag){
            diagnosis_text = diag.diag_name;
        } else {
            diagnosis_text = this.diag_text;
        }

        let info: any = {
            cid: this.cid,
            title: this.title,
            first_name: this.first_name,
            middle_name: this.middle_name,
            last_name: this.last_name,
            gender: this.gender,
            gender_name: this.gender_name,
            dob: this.dob,
            age: this.age,
            patient_address_name: this.patient_address_name,
            phone_number: this.phone_number,
            diag_text: diagnosis_text,
            refer_hcode: this.refer_hcode,
            refer_to_hcode: this.refer_to_hcode,
            refer_level: this.refer_level,
            patient_status: this.patient_status,
            refer_cause: '',
            evaluate_cause: this.evaluate_clause_check.toString(),
            extra_detail: this.extra_detail,
            patient_right: this.patient_right,
            patient_right_code: this.patient_right_code,
            discharge_date: this.discharge_date,
            visit_date: moment(this.visit_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
            // moment(this.visit_date).tz('Asia/Bangkok').format('YYYY-MM-DD');
            // add on coc register
            is_smoke: this.is_smoke,
            department: this.department,
            village_code: this.villageCode,
            house_no : this.house_no,
            items: this.items.toString(),
        };
        this.itemProfile.hno = this.house_no;
        this.itemProfile.villno = this.villno;
        this.itemProfile.villcode = this.villageCode;
        this.itemProfile.villname = this.villageName;
        coc_appointment.patient = info;
        // console.log('fon ',info);
        coc_appointment.profile = this.itemProfile;
        coc_appointment.refer_no = this.refer_no;

        if (this.refer_hcode == '' || this.refer_hcode == null) {
            this.sweetAlertService.error(
                'คำชี้แจง',
                'ไม่ระบุรหัสหน่วยบริการที่ส่งต่อ',
                'SmartRefer Ubon'
            );
            return;
        }

        if (this.refer_to_hcode == '' || this.refer_to_hcode == null) {
            this.sweetAlertService.error(
                'คำชี้แจง',
                'ไม่ระบุรหัสหน่วยบริการเยี่ยมบ้าน',
                'SmartRefer Ubon'
            );
            return;
        }
        /* if not get diag
        if (this.diagnosis.length == 0) {
            this.sweetAlertService.error(
                'คำชี้แจง',
                'กรุณาระบุการวินิจฉัยโรค',
                'SmartRefer Ubon'
            );
            return;
        }

        if(this.diagnosis.find((x: any) => x.diag_type == 1) == undefined){
            this.sweetAlertService.error(
                'คำชี้แจง',
                'กรุณาระบุการวินิจฉัยโรคหลัก',
                'SmartRefer Ubon'
            );
            return;
        }
        */

        if(this.villageCode.length != 8){
            this.sweetAlertService.error(
                'คำชี้แจง',
                'กรุณาระบุที่อยู่ใหม่ เนื่องจากไม่พบรหัสหมู่บ้าน',
                'SmartRefer Ubon'
            );
            return;
        }

        if (!this.refer_level) {
            this.sweetAlertService.error(
                'คำชี้แจง',
                'กรุณาระบุความเร่งด่วน',
                'SmartRefer Ubon'
            );
            return;
        }

        if (!this.patient_status) {
            this.sweetAlertService.error(
                'คำชี้แจง',
                'กรุณาระบุสถานะผู้ป่วยขณะจำหน่าย',
                'SmartRefer Ubon'
            );
            return;
        }

        if (this.refer_level && this.patient_status) {
            coc_appointment.medicine = this.med_reconcile_checked;
            coc_appointment.lab = this.lab_checked;
            coc_appointment.xray = this.xray_checked;
            coc_appointment.allergy = this.allergy_checked;
            coc_appointment.diagnosis = this.diagnosis;
            coc_appointment.equipments = equipments;

            // console.log(coc_appointment);
            let rs: any;
            try {
                rs = await this.registerService.save(coc_appointment);
                // console.log('res:', coc_appointment);
            } catch (error) {
                console.log(error);                
                this.sweetAlertService.success(
                    'คำชี้แจง',
                    'ไม่สามารถบันทึกส่งเยี่ยมบ้านได้',
                    'Smart COC'
                );
            }

            if (rs.length > 0) {

                this.med_reconcile_checked.forEach(async (e: any) => {
                    if(e.usage_line1 == null || e.usage_line1 == undefined || e.usage_line1 == ''){
                        e.usage_line1 = '';
                    }
                    if(e.usage_line2 == null || e.usage_line2 == undefined || e.usage_line2 == ''){
                        e.usage_line2 = '';
                    }
                    if(e.usage_line3 == null || e.usage_line3 == undefined || e.usage_line3 == ''){
                        e.usage_line3 = '';
                    }
                    let med_usage = e.usage_line1 + e.usage_line2 + e.usage_line3;
                    let data: any = {
                        coc_register_id: rs[0],
                        dispend_date: e.date_serv,
                        medicine_name: e.drug_name,
                        medicine_use: med_usage,
                        qty: e.qty,
                    };

                    let rs_med: any = await this.registerService.save_med(data);
                });

                this.lab_checked.forEach(async (e: any) => {
                    let data: any = {
                        coc_register_id: rs[0],
                        lab_datetime: e.date_serv + ' ' + e.time_serv,
                        lab_test: e.lab_name,
                        lab_result: e.lab_result,
                        lab_normal: e.standard_result,
                    };

                    let rs_lab: any = await this.registerService.save_lab(data);
                });

                this.xray_checked.forEach(async (e: any) => {
                    let data: any = {
                        coc_register_id: rs[0],
                        xray_datetime: e.xray_date,
                        xray_name: e.xray_name,
                    };

                    let rs_xray: any = await this.registerService.save_xray(
                        data
                    );
                });

                this.allergy_checked.forEach(async (e: any) => {
                    let data: any = {
                        coc_register_id: rs[0],
                        hcode: this.hcode,
                        drug_name: e.drug_name,
                        symptom: e.symptom,
                        begin_date: e.daterecord,
                    };

                    let rs_allergy: any = await this.registerService.save_allergy(data);
                });

                this.diagnosis.forEach(async (e: any) => {
                    let data: any = {
                        coc_register_id: rs[0],
                        diag_code: e.diag_code,
                        diag_name: e.diag_name,
                        diag_type: e.diag_type,
                    };

                    let rs_diag: any = await this.registerService.save_diag(data);
                });

                this.sweetAlertService.success(
                    'คำชี้แจง',
                    'บันทึกข้อมูลสำเร็จ',
                    'SmartRefer Ubon'
                );
                await this.router.navigate(['/register']);
                this.sweetAlertService.success(
                    'คำชี้แจง',
                    'บันทึกลงทะเบียนสำเร็จ',
                    'Smart COC'
                );
            }
        } else {
            this.sweetAlertService.error(
                'คำชี้แจง',
                'ไม่ได้ระบุความเร่งด่วน/สถานะผู้ป่วยขณะจำหน่าย',
                'Smart COC'
            );
        }
    }

    // action when checkbox med reconcile change
    async med_reconcile(i: any, e: any) {
        if (e.checked) {
            this.med_reconcile_checked.push(i);
        } else {
            this.removeItemArray(this.med_reconcile_checked, i);
        }
    }

    // action when checkbox lab change
    async lab(i: any, e: any) {
        if (e.checked) {
            this.lab_checked.push(i);
        } else {
            this.removeItemArray(this.lab_checked, i);
        }
    }

    // action when checkbox xray change
    async xray(i: any, e: any) {
        if (e.checked) {
            this.xray_checked.push(i);
        } else {
            this.removeItemArray(this.xray_checked, i);
        }
    }

    // action when checkbox allergy change
    async allergy(i: any, e: any) {
        if (e.checked) {
            this.allergy_checked.push(i);
        } else {
            this.removeItemArray(this.allergy_checked, i);
        }
    }

    // search hospital
    search() {
        // console.log(this.list_all_hospital);
        // console.log(this.searchHospital);
        let is_number: boolean = false;
        if (!isNaN(Number(this.searchHospital))) {
            is_number = true;
        }
        if (this.searchHospital.length == 5 && is_number) {
            this.refer_to_hcode = this.searchHospital;
            let rs: any = this.list_all_hospital.filter((hospital: any) =>
                hospital.code.includes(this.searchHospital)
            );
            if (rs.length == 1) {
                this.refer_to_hcode = rs[0].code;
                this.filteredHospital = rs;
            }
        } else if (!is_number && this.searchHospital.length > 3) {
            //search from lis all hospital
            this.filteredHospital = this.list_all_hospital.filter((hospital: any) =>
                hospital.name.toLowerCase().includes(this.searchHospital.toLowerCase())
            );
            if (this.filteredHospital.length >= 1) {
                this.refer_to_hcode = this.filteredHospital[0].code;
            }
            // console.log('search from lis all hospital', this.filteredHospital);
        } else {
            this.refer_to_hospname = '';
            this.filteredHospital = [];
        }
        // console.log(this.filteredHospital);

    }

    // select hospital
    onSelectionChange(event: any): void {

        this.is_select_hospcode = true;
        this.refer_to_hcode = event.value;
        // console.log(this.refer_to_hcode);
    }

    // select department
    onSelectionDepartmentChange(event: any): void {

        this.department = event.value;
        // console.log(this.department);
    }

    // edit patient
    editPatient() {
        const dialogRef = this.dialog.open(EditDialog, {
            width: '480px',
            data: {
                cid: this.cid,
                title: this.title,
                first_name: this.first_name,
                last_name: this.last_name,
                gender_name: this.gender_name,
                patient_address_name: this.patient_address_name,
                phone_number: this.phone_number,
                villageCode: this.villageCode,
                house_no: this.house_no,
            },
        });

        dialogRef.afterClosed().subscribe(async (result) => {
            if (result) {
                let address_his: any;
                const rs: any = await this.thaiaddressService.select_full(
                    result.villageCode.substring(0, 2),
                    result.villageCode.substring(2, 4),
                    result.villageCode.substring(4, 6),
                    result.villageCode.substring(6, 8)
                );
                if (rs.length > 0) {
                    address_his = rs[0].full_name;
                } else {
                    address_his = 'ไม่พบข้อมูล';
                }
                this.patient_address_name = result.house_no + ' ' + address_his;
                this.phone_number = result.phone_number;
                this.villageCode = result.villageCode;
                this.house_no = result.house_no;
                this.villageName = result.villname;
                this.villno = Number(result.villno).toString();
            }
        });
    }

    // select Items
    selectItem(data: any, e: any) {
        if (e.checked) {
            this.items.push(data);
        } else {
            this.items.splice(this.items.indexOf(data), 1);
        }
    }

    private _filter(value: string): any[] {
        const filterValue = value.toLowerCase();
        return this.list_all_hospital.filter(
            (option) => option.name.toLowerCase().includes(filterValue)
        );
    }

    // get icd10
    onOptionSelected(event: any) {
        // console.log(event);
        this.selectedDiagnosisCode = event.option.value.id;
        // console.log(this.selectedDiagnosisCode);
    }

    // add diagnosis
    addDiagnosis() {
        // const value:any = this.formControl.value;
        if (this.icd10 == '' || this.diag_text == '' || this.selectedDiagnosisType == '') {
            this.sweetAlertService.error(
                'คำชี้แจง',
                'กรุณาระบุข้อมูลให้การวินิจฉัย ICD10 ให้ครบถ้วน',
                'SmartRefer Ubon'
            );
            return;
        }
        const icd10code = this.listIcd10Who.find((x: any) => x.name == this.icd10);
        let diag_code = icd10code || { id: '' };

        let diag = {
            diag_code: diag_code.id,
            diag_name: this.diag_text,
            diag_type: this.selectedDiagnosisType
        }
        this.diagnosis.push(diag);
        this.formControl.setValue('');
        this.icd10 = '';
        this.diag_text = '';
        this.selectedDiagnosisType = '';
        // console.log(this.diagnosis);
    }

    // remove diagnosis
    removeDiagnosis(i: any) {
        this.removeItemArray(this.diagnosis, i);
    }

    // filter department
    filterDepartment(value: string): void {
        // console.log('key search department:', value);
        this.filteredDepartment = this.list_department.filter(department => 
            department.dep_name.toLowerCase().includes(value.toLowerCase())
        );
    }

    // filter icd10
    filterIcd10Who(value: string): void {
        // console.log('key search icd10:', value);
        if(value.length >= 3) {
            this.filteredDiagnosis = this.listIcd10Who.filter(icd10 => 
                icd10.name.toLowerCase().includes(value.toLowerCase()) || icd10.id.toLowerCase().includes(value.toLowerCase())
            );    
        }
    }

}
