import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PatientInfo } from '../edit-data';
import { ThaiaddressService } from '../../../../services/thaiaddress.service';


@Component({
    selector: 'dialog-edit',
    templateUrl: './edit.component.html',
    encapsulation: ViewEncapsulation.None
})

export class EditDialog {

    patient: any = [];
    cid: string;
    title: string;
    first_name: string;
    last_name: string;
    gender_name: string;
    patient_address_name: string;
    phone_number: string;
    villageCode: string;
    house_no: string;

    listProvince: any = [];
    listAmphur: any = [
    ];
    listTambon: any = [
    ];
    listMooban: any = [
    ];

    filterAmpur: any = [];
    filterTambon: any = [];
    filterMooban: any = [];
    provinceCode: string;
    amphurCode: string;
    tambonCode: string;
    moobanCode: string;
    villageName: string;

    constructor(
        public matDialogRef: MatDialogRef<PatientInfo>,
        private thaiaddressService: ThaiaddressService,
        @Inject(MAT_DIALOG_DATA) public data: PatientInfo,
    ) { 
        this.cid = this.data.cid;
        this.title = this.data.title;
        this.first_name = this.data.first_name;
        this.last_name = this.data.last_name;
        this.gender_name = this.data.gender_name;
        this.patient_address_name = this.data.patient_address_name;
        this.phone_number = this.data.phone_number;
        this.villageCode = this.data.villageCode;
        this.house_no = this.data.house_no;

        this.provinceCode = this.data.villageCode.substring(0, 2);
        this.amphurCode = this.data.villageCode.substring(2, 4);
        this.tambonCode = this.data.villageCode.substring(4, 6);
        this.moobanCode = this.data.villageCode.substring(6, 8);
        console.log(this.moobanCode);
    }

    async ngOnInit() {

        await this.getProvince();
        await this.getDistrict(this.provinceCode);
        await this.getSubdistrict(this.provinceCode, this.amphurCode);
        await this.getVillage(this.provinceCode, this.amphurCode, this.tambonCode);
    }

    /**
     * Save and close
     */
    saveAndClose(): void {
        let result = {
            "phone_number": this.phone_number,
            "villageCode": this.provinceCode + this.amphurCode + this.tambonCode + this.moobanCode,
            "house_no": this.house_no,
            "villname": this.villageName,
            "villno": this.moobanCode,
        }
        console.log(result);          
        this.matDialogRef.close(result);
    }

    getAmphur(event: any) {
        this.provinceCode = event.value;
        this.getDistrict(this.provinceCode);
        this.filterTambon = [];
        this.filterMooban = [];
    }

    getTambon(event : any) {
        this.amphurCode = event.value;
        this.getSubdistrict(this.provinceCode, this.amphurCode);
        this.filterMooban = [];
    }

    getMooban(event : any) {
        this.tambonCode = event.value;
        this.getVillage(this.provinceCode, this.amphurCode, this.tambonCode);
    }

    saveMooban(event : any) {
        this.moobanCode = event.value;
        this.villageName = this.filterMooban.find((x: any) => x.code == this.moobanCode).name;
    }

    async getProvince() {
        await this.thaiaddressService.getProvince().then((res: any) => {
            this.listProvince = res;
            this.listProvince.sort((a: any, b: any) => {
                if (a.name < b.name) {
                    return -1;
                }
                if (a.name > b.name) {
                    return 1;
                }
                return 0;
            });
        }).catch((err: any) => {
            console.log(err);
        });
        // console.log(this.listProvince);
    }

    async getDistrict(province: any) {
        await this.thaiaddressService.getDistrict(province).then((res: any) => {
            this.listAmphur = res;
            this.filterAmpur = this.listAmphur;
        }).catch((err: any) => {
            console.log(err);
        });
        // console.log(this.listAmphur);
    }

    async getSubdistrict(province: any, district: any) {
        await this.thaiaddressService.getSubdistrict(province, district).then((res: any) => {
            this.listTambon = res;
            this.filterTambon = this.listTambon;
        }).catch((err: any) => {
            console.log(err);
        });
        // console.log(this.listTambon);
    }

    async getVillage(province: any, district: any, subdistrict: any) {
        await this.thaiaddressService.getVillage(province, district, subdistrict).then((res: any) => {
            this.listMooban = res;
            this.filterMooban = this.listMooban;
        }).catch((err: any) => {
            console.log(err);
        });
        // console.log(this.listMooban);
    }

    /**
     * Discard the message
     */
    discard(): void {

    }

    /**
     * Save the message as a draft
     */
    saveAsDraft(): void {

    }

    /**
     * Send the message
     */
    send(): void {

    }

}