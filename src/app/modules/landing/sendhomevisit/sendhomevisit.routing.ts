import { Route } from '@angular/router';
import {SendhomevisitComponent } from 'app/modules/landing/sendhomevisit/sendhomevisit.component';

export const sendhomevisitRoutes: Route[] = [
    {
        path     : '',
        component: SendhomevisitComponent
    }
];
