import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { RegisterComponent } from 'app/modules/landing/register/register.component';
import { registerRoutes } from 'app/modules/landing/register/register.routing';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';

@NgModule({
    declarations: [
        RegisterComponent
    ],
    imports     : [
        RouterModule.forChild(registerRoutes),
        MatPaginatorModule,
        SharedModule,
        NgxDaterangepickerMd.forRoot()
    ]
})
export class RegisterModule
{

}
