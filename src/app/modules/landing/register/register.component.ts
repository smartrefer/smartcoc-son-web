import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { RegisterService } from '../../../services/register.service';
import { ThaiaddressService } from '../../../services/thaiaddress.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { SweetAlertService } from '../../../shared/sweetalert.service';
import { Register } from './register';

import moment from "moment";
import * as _ from "lodash";

@Component({
    selector: 'landing-home',
    templateUrl: './register.component.html',
    encapsulation: ViewEncapsulation.None
})

export class RegisterComponent {

    // ปฏิทิน
    selectedDateStart: any = { sdate: null, edate: null };selectedDateEnd: any = { sdate: null, edate: null };
    sdate: any;
    edate: any;
    validateForm: boolean = false;
    displayedColumns: string[] = ['pcucode', 'name', 'address', 'phone', 'dchdate', 'prediag', 'action'];
    dataSource: any;
    dataSourecFind: any;
    rowData: any[] = [];
    rowDataFind: any[] = [];

    hn: any;
    hcode: any = localStorage.getItem('hcode');
    list_all_hospital:any = [];

    is_hospital_loaded: boolean = true;
    is_diag_loaded: boolean = true;

    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(
        private router: Router,
        private registerService: RegisterService,
        private thaiaddressService: ThaiaddressService,
        private spinner: NgxSpinnerService,
        private sweetAlertService: SweetAlertService,
    ) { 
        let username = sessionStorage.getItem('username');
        if(!username){
            this.router.navigate(['/sign-in']);
        }
        let hcode = localStorage.getItem('list_all_hospital');
        const list_hosp = JSON.parse(hcode);
        if(list_hosp){
            this.is_hospital_loaded = false;
            // console.log('list_hosp',list_hosp);
        } 

        let icd10 = sessionStorage.getItem('list_icd10');
        const list_diag = JSON.parse(icd10);
        if(list_diag){
            this.is_diag_loaded = false;
            // console.log('list_diag',list_diag);
        }

    }

    async ngAfterViewInit() {
        this.select_register();
        // console.log('is_hospital_loaded',this.is_hospital_loaded);
        // console.log('is_diag_loaded',this.is_diag_loaded);
        if(this.is_hospital_loaded){
            // console.log('getAllHospital');
            await this.getAllHospital();
        }
        /*
        if(this.is_diag_loaded){
            console.log('getIcd10');
            await this.getIcd10();
        }
        */
    }

    async select_register() {
        this.spinner.show();
        try {
            let info: any = {
                "hcode": this.hcode
            }
            let rs: any = await this.registerService.select_register(info);

            this.rowData = rs; //รับข้อมูลที่ทำการ pack เรียบร้อย
            this.rowDataFind = this.rowData;
            this.dataSource = new MatTableDataSource<Register>(this.rowData);
            this.dataSource.paginator = this.paginator;
            // console.log(this.dataSource);
            
            this.spinner.hide();
        } catch (error) {
            console.log(error.error);
            this.spinner.hide();
            this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถแสดงข้อมูลได้.. กรุณากด refresh', 'Smart Refer Ubon');
        }
    }

    async register() {

        if (this.hn) {
            let address_his: any;
            let rs: any = await this.registerService.select_coc_patient(this.hn)
            if (rs.profile.length > 0) {
                let rs_address: any = await this.thaiaddressService.select_full(rs.profile[0].chwpart, rs.profile[0].amppart, rs.profile[0].tmbpart, rs.profile[0].moopart)
                if (rs_address.length > 0) {
                    address_his = rs_address[0].full_name
                }
                else {
                    address_his = 'ไม่พบข้อมูล';
                }
                let item: any = {
                    "hn": rs.profile[0].hn,
                    "refer_hospcode": this.hcode,
                    "refer_hospname": null,
                    "refer_to_hospcode": this.hcode,
                    "refer_to_hospname": null,
                    "cid": rs.profile[0].cid,
                    "title": rs.profile[0].title,
                    "first_name": rs.profile[0].first_name,
                    "last_name": rs.profile[0].last_name,
                    "age": rs.profile[0].age,
                    "dob": rs.profile[0].brthdate,
                    "patient_address_name": rs.profile[0].addrpart + '  ' + address_his,
                    "tel": rs.profile[0].contact_mobile,
                    "patient_right": rs.profile[0].pttype_name,
                    "patient_type_code": rs.profile[0].pttype_no,
                    "discharge_date": null,
                    "diagnote": null
                }

                sessionStorage.setItem('hn', this.hn);
                sessionStorage.setItem('itemStorage', JSON.stringify(item))
                this.router.navigate(['/sendhomevisit']);
            }
            else {
                this.sweetAlertService.error('คำชี้แจง', 'ไม่พบข้อมูล HN ' + this.hn + ' ในระบบฐานข้อมูล', 'SmartRefer Ubon');
            }
        }
    }

    async datalist(i: any) {
        if (i) {
            sessionStorage.setItem('hn', i.hn);
            sessionStorage.setItem('itemStorage', JSON.stringify(i))
            this.router.navigate(['/sendhomevisit']);
        }
    }

    async search() {
        // let start_date = new Date(this.sdate);
        // let end_date = new Date(this.edate);

        // this.dataSource.data = this.dataSource.data.filter(e =>
        //     new Date(e.discharge_date).getTime() >= start_date.getTime() &&
        //     new Date(e.discharge_date).getTime() <= end_date.getTime()
        // );
        let start_date = this.sdate;
        let end_date = this.edate;
        let hcode = this.hcode;
        //หาข้อมูลตามวันที่ discharge_date
        this.dataSource = new MatTableDataSource<Register>(this.rowDataFind);
        this.dataSource.data = this.dataSource.data.filter(e =>
            moment(e.discharge_date).format('YYYY-MM-DD') >= moment(start_date).format('YYYY-MM-DD') &&
            moment(e.discharge_date).format('YYYY-MM-DD') <= moment(end_date).format('YYYY-MM-DD')
            );
        this.dataSource.paginator = this.paginator;
        // console.log(start_date);
        
    }

    async clear() {
        this.sdate = null;
        this.edate = null;
        this.hn = null;
        this.dataSource = null;
        this.select_register();
    }
    dateChangeStart(e:any) {
        // console.log("date change start");
        // console.log(e);
        if (e.startDate != null) {
          let syear = e.startDate.$y;
          let smonth = String(e.startDate.$M + 1).padStart(2, "0");
          let sday = String(e.startDate.$D).padStart(2, "0");
          this.sdate = [syear, smonth, sday].join("-");
        //   console.log(this.sdate);
        }
      }
      dateChangeEnd(e:any) {
        // console.log("date change end");
        // console.log(e);
        if (e.startDate != null) {
          let eyear = e.endDate.$y;
          let emonth = String(e.endDate.$M + 1).padStart(2, "0");
          let eday = String(e.endDate.$D).padStart(2, "0");
          this.edate = [eyear, emonth, eday].join("-");
        //   console.log(this.edate);
        }
      }

    
    async getAllHospital() {
        await localStorage.removeItem('list_all_hospital');
        let rs: any = await this.thaiaddressService.select_all();
        // console.log(rs);
        this.list_all_hospital = rs;
        await localStorage.setItem('list_all_hospital', JSON.stringify(rs));
    }

    async getIcd10() {
        await sessionStorage.removeItem('list_icd10');
        let rs: any = await this.thaiaddressService.getIcd10();
        // console.log('diag',rs);
        await sessionStorage.setItem('list_icd10', JSON.stringify(rs));
    }

}
