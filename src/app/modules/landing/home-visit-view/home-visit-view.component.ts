import { Component, ViewChild, ViewEncapsulation, Inject } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { HomevisitService } from '../../../services/homevisit.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { round } from 'lodash';

export interface FuseConfirmationConfig {
    title?: string;
    message?: string;
    icon?: {
        show?: boolean;
        name?: string;
        color?:
        | 'primary'
        | 'accent'
        | 'warn'
        | 'basic'
        | 'info'
        | 'success'
        | 'warning'
        | 'error';
    };
    actions?: {
        confirm?: {
            show?: boolean;
            label?: string;
            color?:
            | 'primary'
            | 'accent'
            | 'warn';
        };
        cancel?: {
            show?: boolean;
            label?: string;
        };
    };
    dismissible?: boolean;
}

@Component({
    selector: 'landing-home',
    templateUrl: './home-visit-view.component.html',
    encapsulation: ViewEncapsulation.None
})

export class HomeVisitViewComponent {
    sdate: any;
    edate: any;

    patient: any;
    result: any;

    // summary: string;

    coc_register_id: any;
    home_visit_date: any = new Date();
    present_gender: any;

    age: any;
    patient_address_name: any;
    phone_number: any;
    diag_text: any;
    evaluate_clause: any;
    extra_detail: any;
    patient_right_code: any;
    disablility_card: any;
    address_lat: any;
    address_long: any;
    care_giver_name: any;
    care_giver_contact: any;
    coc_status: any;
    gait_aid: any;
    home_item: any = [];
    med_use: any;
    med_use_problem: any;
    self_use: any;
    self_use_list: any;
    durgs_use: any;
    drugs_use_other: any;
    summary: any;
    problem_and_comment: any;
    result_code: any;
    result_name: any;
    plan_code: any;
    plan_name: any;
    body_temp: any;
    pulse_rate: any;
    respiratory_rate: any;
    sbp: any;
    dbp: any;
    o2sat: any;
    body_height: any;
    body_weight: any;
    bmi: any;
    dtx: any;

    error_message: any = [];

    item = [
        { id: 1, name: 'Home Oxygen' },
        { id: 2, name: 'NG Tube' },
        { id: 3, name: 'Foley Catheter' },
        { id: 4, name: 'PCA' },
        { id: 5, name: 'IV Fluid' },
        { id: 6, name: 'Colostomy Bag' },
        { id: 7, name: 'CAPD' },
        { id: 8, name: 'Tracheostomy Tube' },
    ]


    comment_text : string = '';

    result2q_score: any = '';

    /**
     * Constructor
     */
    constructor(
        private router: Router,
        private spinner: NgxSpinnerService,
        private homevisitService: HomevisitService,
    ) {
        let username = sessionStorage.getItem('username');
        if(!username){
            this.router.navigate(['/sign-in']);
        }
    }

    ngOnInit() {
        this.load_data();
    }

    async back() {
        this.router.navigate(['/coc']);
    }

    async load_data() {
        try {
            this.patient = JSON.parse(sessionStorage.getItem('itemRegister'));
            this.coc_register_id = this.patient.coc_register_id;
            this.home_visit_date = this.patient.home_visit_date;
        } catch (error) {
            console.log(error.error);
        }
    }

    home_visit(data: any, e: any) {
        if (e.checked) {
            this.home_item.push(data);
        } else {
            this.home_item.splice(this.home_item.indexOf(data), 1);
        }
        console.log(this.home_item);
    }

}
