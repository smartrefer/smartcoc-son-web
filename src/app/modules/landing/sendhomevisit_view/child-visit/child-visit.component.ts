import { Router, Navigation } from '@angular/router';
import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { DialogData } from '../dialog-data';

@Component({
    selector: 'landing-evaluate-child-visit',
    templateUrl: './child-visit.component.html'
    
})

export class ChildVisitDialog {
    userFullname: string;
    lastVisit: string;
    validateForm: boolean = false;
    evaluate :any;
    title : string;

    profilePatient: any;

    constructor(
        private router: Router,
        public matDialogRef: MatDialogRef<ChildVisitDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) {
        this.userFullname = localStorage.getItem('userFullname');
        this.profilePatient = JSON.parse(localStorage.getItem('profileData'));
    }

    ngOnInit(): void {
        this.title = this.data.evaluate.question_text;
        this.evaluate = this.data.evaluate;
        this.lastVisit = this.data.lastVisit;
        //console.log(this.evaluate);
    }

    close(): void {
        this.matDialogRef.close();
    }

}
