import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ThaiaddressService } from '../../../services/thaiaddress.service';
import { RegisterService } from '../../../services/register.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { SweetAlertService } from '../../../shared/sweetalert.service';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { Register } from './register';

import moment from "moment";
import * as _ from "lodash";
import { DialogTransfer } from './dialog-transfer/dialog-transfer.component';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'landing-home',
    templateUrl: './coc.component.html',
    encapsulation: ViewEncapsulation.None
})
export class CocComponent {

    ranges: any = {
        Today: [moment(), moment()],
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 7 Days": [moment().subtract(6, "days"), moment()],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "This Month": [moment().startOf("month"), moment().endOf("month")],
        "Last Month": [
            moment().subtract(1, "month").startOf("month"),
            moment().subtract(1, "month").endOf("month"),
        ],
    };

    rowData: any[] = [];
    rowDataFind: any[] = [];
    dataSource: any;
    dataSourecFind: any;
    rowDataFindFullname: any[] = [];
    dataSourecFindFullname: any;
    itemCatagory: any = [];
    itemItems: any = [];

    // ปฏิทิน
    selectedDateStart: any = { sdate: null, edate: null }; selectedDateEnd: any = { sdate: null, edate: null };
    sdate: any;
    edate: any;
    fullname: any;
    validateForm: boolean = false;
    hcode: any = localStorage.getItem('hcode');
    list_all_hospital: any = [];
    is_hospital_loaded: boolean = true;
    displayedColumns: string[] = ['pcucodesend', 'urgency', 'ptstatus', 'name', 'address', 'phone', 'dchdate', 'prediag', 'typerefer', 'last_visit', 'refer_to_pcuname', 'action'];
    isSearch: boolean = false;
    searchData: any = [];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    /**
     * Constructor
     */
    constructor(
        private router: Router,
        private registerService: RegisterService,
        private spinner: NgxSpinnerService,
        private sweetAlertService: SweetAlertService,
        private thaiaddressService: ThaiaddressService,
        public dialog: MatDialog,
    ) {
        let username = sessionStorage.getItem('username');
        if (!username) {
            this.router.navigate(['/sign-in']);
        }
        let hcode = localStorage.getItem('list_all_hospital');
        const list_hosp = JSON.parse(hcode);
        if(list_hosp){
            this.is_hospital_loaded = false;
            // console.log('list_hosp',list_hosp);
        } 
        sessionStorage.setItem('route', '/coc');
        // console.log(sessionStorage.getItem('search'));
        
        if (sessionStorage.getItem('search')) {
            let search = JSON.parse(sessionStorage.getItem('search'));
            this.sdate = search.start_date;
            this.selectedDateStart = {
                startDate : new Date(search.start_date),
                endDate : new Date(search.start_date)
            };
            this.edate = search.end_date;
            this.selectedDateEnd = {
                startDate : new Date(search.end_date),
                endDate : new Date(search.end_date)
            };
            this.hcode = search.hcode;
            this.isSearch = true;
        }
        // console.log(this.selectedDateStart, this.selectedDateEnd)
    }

    ngOnInit(): void {
        this.spinner.show();

        this.select_coc();
        if (this.is_hospital_loaded) {
            this.getAllHospital();
        }
    }

    async list_catagory() {
        try {
            await this.registerService.list_catagory().then((res: any) => {
                this.itemCatagory = res;
            });
        } catch (error) {
            console.log(error.error);
        }
    }

    async select_coc() {

        // this.spinner.show();
        await this.list_catagory()
        // console.log("this.isSearch",this.isSearch);
            try {
                let info = {
                    "hcode": this.hcode
                }
                await this.registerService.select_coc(info).then((res: any) => {
                    for (let i = 0; i < res.length; i++) {
                        res[i].d_update = new Date(res[i].d_update);
    
                        let clause = res[i].evaluate_cause.split(',');
                        let catagory_name = '';
                        for (let j = 0; j < clause.length; j++) {
                            let catagory: any = this.itemCatagory.find(e => e.catagory_id == clause[j]);
                            if (catagory) {
                                catagory_name += catagory.catagory_name + ',';
                            } else {
                                catagory_name = '';
                            }
                        }
                        res[i].clause = catagory_name.substring(0, catagory_name.length - 1);
                    }
                    this.rowData = res;
                    this.rowDataFind = this.rowData;
                    this.rowDataFindFullname = this.rowData;
                    this.dataSource = new MatTableDataSource<Register>(this.rowData);
                    this.dataSource.paginator = this.paginator;
                    if(this.isSearch){
                        this.search();
                    }
                    this.spinner.hide();
                });
    
            } catch (error) {
                console.log(error);
                this.spinner.hide();
            } 
            // this.spinner.hide(); 
    }

    async sendhomevisitview(i: any) {
        // console.log('xxx:'+JSON.stringify(i));

        if (i) {
            localStorage.setItem('profileData', '');
            sessionStorage.setItem('itemStorage', '');
            localStorage.setItem('profileData', JSON.stringify(i));
            sessionStorage.setItem('itemStorage', JSON.stringify(i));
            // console.log(localStorage.getItem('profileData'));
            // console.log(sessionStorage.getItem('itemStorage'));

            this.router.navigate(['/sendhomevisitview']);
        }
    }
    async homevisit(i: any) {
        if (i) {
            sessionStorage.setItem('itemRegister', JSON.stringify(i))
            this.router.navigate(['/homevisit']);
        }
    }
    async new_homevisit(i: any) {
        // console.log('xxx:'+JSON.stringify(i));
        if (i) {
            sessionStorage.setItem('itemStorage', '');
            sessionStorage.setItem('itemRegister', '');
            sessionStorage.setItem('itemRegister', JSON.stringify(i))
            sessionStorage.setItem('itemStorage', JSON.stringify(i))
            this.router.navigate(['/homevisit']);
        }
    }
    async evaluate(profileData) {
        localStorage.setItem('profileData', JSON.stringify(profileData));
        this.router.navigate(['/evaluate']);
    }
    async result(i: any) {
        if (i) {
            sessionStorage.setItem('itemRegister', JSON.stringify(i))
            this.router.navigate(['/result']);
        }
    }


    async search() {
        this.fullname = null;
        let start_date = this.sdate;
        let end_date = this.edate;
        let hcode = this.hcode;
        //หาข้อมูลตามวันที่ d_update
        this.dataSource = new MatTableDataSource<Register>(this.rowDataFind);
        this.dataSource.data = this.dataSource.data.filter(e =>
            moment(e.d_update).format('YYYY-MM-DD') >= moment(start_date).format('YYYY-MM-DD') &&
            moment(e.d_update).format('YYYY-MM-DD') <= moment(end_date).format('YYYY-MM-DD')
        );
        this.dataSource.paginator = this.paginator;
        this.isSearch = true;
        this.searchData = this.dataSource;
        let search = {
            start_date: start_date,
            end_date: end_date,
            hcode: hcode
        }
        sessionStorage.setItem('search', JSON.stringify(search));
    }

    async search_fullname() {
        //หาข้อมูลตามวันที่อัพเดทข้อมูล
        this.rowDataFindFullname = this.rowData;

        if (this.fullname) {
            //หาข้อมูลชื่อและนามสกุล
            this.dataSource = new MatTableDataSource<Register>(this.rowDataFindFullname);
            this.dataSource.data = this.dataSource.data.filter(e => _.includes(e.first_name, this.fullname) || _.includes(e.last_name, this.fullname) || _.includes(e.refer_hospname, this.fullname));
            this.dataSource.paginator = this.paginator;
            // console.log("xxx", this.dataSource.data);

        }
        else {
            this.sweetAlertService.error('คำชี้แจง', 'ไม่พบข้อมูล ' + this.fullname + ' ในระบบฐานข้อมูล', 'SmartRefer Ubon');
        }

    }

    async transfer(e: any) {
        // console.log(e);
        if (e) {
            sessionStorage.setItem('itemStorage', JSON.stringify(e));
            sessionStorage.setItem('route', '/coc');
            sessionStorage.setItem('action', 'transfer');
            this.router.navigate(['/registerlist/edit']);
        }
    }

    async clear() {
        // this.sdate = null;
        // this.edate = null;
        this.fullname = null;
        this.dataSource = [];
        this.select_coc();
        this.isSearch = false;
        this.searchData = [];
        // console.log(this.selectedDateStart);

    }

    dateChangeStart(e: any) {
        // console.log("date change start");
        // console.log(e);
        if (e.startDate != null) {
            let syear = e.startDate.$y;
            let smonth = String(e.startDate.$M + 1).padStart(2, "0");
            let sday = String(e.startDate.$D).padStart(2, "0");
            this.sdate = [syear, smonth, sday].join("-");
            //   console.log(this.sdate);
        }
    }
    dateChangeEnd(e: any) {
        // console.log("date change end");
        // console.log(e);
        if (e.startDate != null) {
            let eyear = e.endDate.$y;
            let emonth = String(e.endDate.$M + 1).padStart(2, "0");
            let eday = String(e.endDate.$D).padStart(2, "0");
            this.edate = [eyear, emonth, eday].join("-");
            //   console.log(this.edate);
        }
    }

    openDialogTransfer(data: any): void {

        const dialogRef = this.dialog.open(DialogTransfer, {
            width: '800px',

            data: { data },
        },);

        // dialogRef.afterClosed().subscribe(result => {
        //     let sum_score = 0;
        //     // console.log('The dialog was closed');
        //     this.result2q = result;
        //     this.result2q.forEach(element => {
        //         sum_score += parseInt(element.answer_score);
        //     });
        //     // console.log(sum_score);
        //     if (sum_score > 0) {
        //         this.result2q_score = '2Q : POSITIVE'
        //         this.openDialog9q();
        //     } else {
        //         this.result2q_score = '2Q : NEGATIVE'
        //     }
        //     // console.log(this.result2q);
        // });
    }

    async getAllHospital() {
        await localStorage.removeItem('list_all_hospital');
        let rs: any = await this.thaiaddressService.select_all();
        // console.log(rs);
        this.list_all_hospital = rs;
        await localStorage.setItem('list_all_hospital', JSON.stringify(rs));
    }
}
