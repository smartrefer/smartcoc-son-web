import { Component, ViewEncapsulation } from '@angular/core';
import { Router,Navigation} from '@angular/router';
import { EvaluateService } from '../../../../services/evaluate.service';
import { SweetAlertService } from '../../../../shared/sweetalert.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'evaluate-violently',
    templateUrl: './violently.component.html',
    
})
export class ViolentlyComponent {
    userFullname: string;
    validateForm: boolean = false;
    rowsQuestions: any;

    question_type_id: string = '5';
    id: any = {
        question_type_id: 5
    };
    question: any[] = [];
    myNgmodel = { items: [] };
    profilePatient: any;   
    evaluate_answer_check: any = [];

    constructor(
        private violentService: EvaluateService,
        private sweetAlertService: SweetAlertService,
        private spinner: NgxSpinnerService,
        private router : Router
    ) {
        this.userFullname = localStorage.getItem('userFullname');
        this.profilePatient = JSON.parse(localStorage.getItem('profileData'));
    }

    ngOnInit(): void {

        this.dataEvaluate();
    }   

    async dataEvaluate() {
        this.spinner.show();
        try {
            let rs: any = await this.violentService.list(this.id);
            console.log(rs);
            this.rowsQuestions = rs.question;

            let dataQ = rs.question;
            // console.log(this.rowsQuestions);

            dataQ.forEach((data: any, index: any) => {
                // key['index'] = val + 1;
                // console.log(data);
                // var jsonRow: object = data;
                var jsonRow: object = {};
                // delete jsonRow['answer'];  
                jsonRow['coc_home_id'] = this.profilePatient.coc_home_id;
                jsonRow['question_type_id'] = data['question_type_id'];
                jsonRow['question_id'] = data['question_id'];
                jsonRow['answer_id'] = '';
                jsonRow['answer_text'] = '';
                jsonRow['note_text'] = '';
                jsonRow['answer_score'] = '';
                jsonRow['record_by'] = '';
              
                jsonRow['ngModel'] = '';

                this.question.push(jsonRow);
            });
            console.log(this.question);
            this.spinner.hide();

        } catch (error: any) {
            console.log(error.error);
            this.spinner.hide();
            this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถแสดงข้อมูลได้..', 'SmartRefer Ubon');
        }

    }

    onRadioChange(e) {
        var ev = e;
        console.log(ev);
        console.log(this.question);
        // console.log(this.question2);

        var answer_id: number = Number(ev.source.id);
        var _question_id: number = Number(ev.source.name);
        // console.log(typeof (_question_id));
        // console.log(_question_id);

        let index = this.question.findIndex(x => x.question_id === _question_id);
        console.log(index);
        this.question[index].answer_id = answer_id;
        this.question[index].answer_score = ev.value;
        this.question[index].answer_text = ev.source._elementRef.nativeElement.firstChild.innerText;
        this.question[index].record_by = this.userFullname;
        this.question[index].ngModel = 'ok';

        // console.log(this.question);

    }
    onKeyIn(e) {
        var ev = e;
        // console.log(ev);

        var _question_id: number = Number(ev.target.name);
   
        let index = this.question.findIndex(x => x.question_id === _question_id);
        // console.log(index);
    
        this.question[index].note_text = ev.target.value;
        this.question[index].record_by = this.userFullname;
        this.question[index].ngModel = 'ok';
        // console.log(this.question);

    }
    onKeyInAnswerText(e) {
        var ev = e;
        // console.log(ev);

        var _question_id: number = Number(ev.target.name);
   
        let index = this.question.findIndex(x => x.question_id === _question_id);
        // console.log(index);
    
        this.question[index].answer_text = ev.target.value;
        this.question[index].record_by = this.userFullname;
        this.question[index].ngModel = 'ok';
        // console.log(this.question);

    }
    onDateSelect(e){
        var ev = e;
        // console.log(ev);

        var _question_id: number = Number(ev.targetElement.name);

        let index = this.question.findIndex(x => x.question_id === _question_id);
        // console.log(index);
        // console.log((ev.target.value).toString());
        

        this.question[index].answer_text = (ev.target.value).toString();
        this.question[index].record_by = this.userFullname;
        this.question[index].ngModel = 'ok';

        // console.log(this.question);
        
    }

    async saveEvaluate() {
        this.spinner.show();
        let info = this.question;
        // console.log(info);
        let validate = this.validateQuestion();
        console.log(validate);
        if (validate) {
            let saveResult:boolean;
            for (let inf of info) {
                delete inf['ngModel'];
                try {
                    let rs: any = await this.violentService.saveEvaluate(inf);
                    saveResult = true;
                    // console.log(rs);

                } catch (error: any) {
                    saveResult = false;
                    console.log(error.error);
                    this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถบันทึกข้อมูลได้', 'SmartRefer Ubon');
                }
            }
            if(saveResult){
                this.sweetAlertService.toastSucces('คำชี้แจง', 'บันทึกข้อมูลเรียบร้อย', 'SmartRefer Ubon');
                this.router.navigate(['/evaluate']);
            }else{
                this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถบันทึกข้อมูลได้', 'SmartRefer Ubon');
            }

        } else {
            this.sweetAlertService.error('คำชี้แจง', 'กรุณากรอกข้อมูลให้ครบ', 'SmartRefer Ubon');

        }
        this.spinner.hide();
    }
    onCheckboxChange(e) {
        var ev = e;
        // console.log(ev);
        // console.log(this.question);
        var _question_id: number = Number(ev.source.id);
        // console.log(_question_id);        
   
        let index = this.question.findIndex(x => x.question_id === _question_id);
        // console.log(index);        
        if (ev.checked) {
          this.evaluate_answer_check.push(ev.source.name);
        } else {
          this.removeItemArray(this.evaluate_answer_check, ev.source.name);
        }        
        // this.evaluate_answer_check.map(Number);
        this.evaluate_answer_check.sort();
        // console.log(this.evaluate_answer_check);        
        this.question[index].answer_text = (this.evaluate_answer_check).toString();      
        this.question[index].record_by = this.userFullname;
        this.question[index].ngModel = 'ok';
        // console.log(this.question);        
        
      }
      removeItemArray(array, item) {
        for (var i in array) {
          if (array[i] == item) {
            array.splice(i, 1);
            break;
          }
        }
      }


    validateQuestion() {
        let validate: boolean = true;

        for (let data of this.question) {
            if (data.ngModel == '') {
                validate = false;
                break;
            }

        }
        return validate;
    }
}




