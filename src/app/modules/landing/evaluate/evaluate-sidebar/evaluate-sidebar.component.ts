import { Component, ViewEncapsulation } from '@angular/core';
import { FuseNavigationItem } from '@fuse/components/navigation/navigation.types';

@Component({
    selector     : 'evaluate-sidebar',
    template     : `
        <div class="py-10">
            <!-- Add any extra content that might be supplied with the component -->
            <div class="extra-content">
                <ng-content></ng-content>
            </div>

            <!-- Fixed demo sidebar -->
            <div class="mx-6 text-3xl font-bold tracking-tighter">เลือกการประเมิน</div>
            <fuse-vertical-navigation
                [appearance]="'default'"
                [navigation]="menuData"
                [inner]="true"
                [mode]="'side'"
                [name]="'demo-sidebar-navigation'"
                [opened]="true"></fuse-vertical-navigation>

           
        </div>
    `,
    styles       : [
        `
            demo-sidebar fuse-vertical-navigation .fuse-vertical-navigation-wrapper {
                box-shadow: none !important;
            }
        `
    ],
    encapsulation: ViewEncapsulation.None
})
export class EvaluateSidebarComponent
{
    menuData: FuseNavigationItem[];

    /**
     * Constructor
     */
    constructor()
    {
        this.menuData = [
            {
                title   : 'Actions',
                subtitle: 'Task, project & team',
                type    : 'group',
                children: [
                    {
                        title: 'Chronic',
                        type : 'basic',
                        icon : 'heroicons_outline:plus-circle',
                        link : '/evaluate/cvd-risk'
                    },
                    {
                        title: 'แบบประเมินจิตเวช',
                        type : 'basic',
                        icon : 'heroicons_outline:user-group',
                        link : '/evaluate/psyche'
                    },
                    {
                        title: 'ความรุนแรง',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/violently'
                    },
                    {
                        title: 'Barthel index score',
                        type : 'basic',
                        icon : 'heroicons_outline:briefcase',
                        link : '/evaluate/barthel'
                    },
                    {
                        title: 'PPS',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/pps'
                    },
                    {
                        title   : 'ESAS',
                        subtitle: 'Assign to a task or a project',
                        type    : 'basic',
                        icon    : 'heroicons_outline:badge-check',
                        link : '/evaluate/esas'
                    },
                    {
                        title: 'ADL',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/adl'
                    },
                    {
                        title: 'Motor Power',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/motor-power'
                    },
                    {
                        title: 'Modified Rankin Scale',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/modified-ranking'
                    },
                    {
                        title: 'Pain Killer',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/pain-killer'
                    },
                    {
                        title: 'คัดกรองภาวะโภชนาการ',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/screen-nutrition'
                    },
                    {
                        title: 'ประเมินภาวะโภชนาการ',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/evaluate-nutrition'
                    },
                    {
                        title: 'Advance Care Plan',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/advance'
                    },
                    {
                        title: 'เยี่ยมหลังคลอดมารดา',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/postnatal'
                    },
                    {
                        title: 'เยี่ยมหลังหลังคลอดทารก',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/child-visit'
                    },
                    {
                        title: 'พัฒนาการเด็ก',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/child-develop'
                    },
                    {
                        title: 'โรคติดต่อ',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/epidem'
                    },
                    {
                        title: 'ผู้สูงอายุ',
                        type : 'basic',
                        icon : 'heroicons_outline:briefcase',
                        link : '/evaluate/elderly'
                    },
                    {
                        title: 'อื่นๆ',
                        type : 'basic',
                        icon : 'heroicons_outline:user-add',
                        link : '/evaluate/other'
                    }
                ]
            },
           /*
            {
                title   : 'Settings',
                type    : 'group',
                children: [
                    {
                        title   : 'General',
                        type    : 'collapsable',
                        icon    : 'heroicons_outline:cog',
                        children: [
                            {
                                title: 'Tasks',
                                type : 'basic'
                            },
                            {
                                title: 'Users',
                                type : 'basic'
                            },
                            {
                                title: 'Teams',
                                type : 'basic'
                            }
                        ]
                    },
                    {
                        title   : 'Account',
                        type    : 'collapsable',
                        icon    : 'heroicons_outline:user-circle',
                        children: [
                            {
                                title: 'Personal',
                                type : 'basic'
                            },
                            {
                                title: 'Payment',
                                type : 'basic'
                            },
                            {
                                title: 'Security',
                                type : 'basic'
                            }
                        ]
                    }
                ]
            },
            */
            {
                type: 'divider'
            }
        ];
    }
}
