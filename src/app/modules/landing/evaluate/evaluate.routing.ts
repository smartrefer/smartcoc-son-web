
import { NgModule } from '@angular/core';

import { Routes,RouterModule } from '@angular/router';
import { EvaluateComponent } from 'app/modules/landing/evaluate/evaluate.component';
import { AdlComponent } from 'app/modules/landing/evaluate/adl/adl.component';
import { AdvanceCarePlanComponent } from 'app/modules/landing/evaluate/advance-care-plan/advance-care-plan.component';
import { BarthelComponent } from 'app/modules/landing/evaluate/barthel/barthel.component';
import { ChildDevelopmentComponent } from 'app/modules/landing/evaluate/child-develop/child-develop.component';
import { ChildVisitComponent } from 'app/modules/landing/evaluate/child-visit/child-visit.component';
import { CvdRiskComponent } from 'app/modules/landing/evaluate/cvd-risk/cvd-risk.component';
import { ElderlyComponent } from 'app/modules/landing/evaluate/elderly/elderly.component';
import { EpidemComponent } from 'app/modules/landing/evaluate/epidem/epidem.component';
import { EsasComponent } from 'app/modules/landing/evaluate/esas/esas.component';
import { EvaluateNutritionComponent } from 'app/modules/landing/evaluate/evaluate-nutrition/evaluate-nutrition.component';
import { ModifiedRankingComponent } from 'app/modules/landing/evaluate/modified-ranking/modified-ranking.component';
import { MotorPowerComponent } from 'app/modules/landing/evaluate/motor-power/motor-power.component';
import { OtherComponent } from 'app/modules/landing/evaluate/other/other.component';
import { PainKillerComponent } from 'app/modules/landing/evaluate/pain-killer/pain-killer.component';
import { PostnatalComponent } from 'app/modules/landing/evaluate/postnatal/postnatal.component';
import { PpsComponent} from 'app/modules/landing/evaluate/pps/pps.component';
import { PsycheComponent } from 'app/modules/landing/evaluate/psyche/psyche.component';
import { ScreenNutritionComponent } from 'app/modules/landing/evaluate/screen-nutrition/screen-nutrition.component';
import { ViolentlyComponent } from 'app/modules/landing/evaluate/violently/violently.component';
import {HomeEvaluateComponent} from './home-evaluate/home-evaluate.component';

const routes: Routes = [
  {
    path:'',
    component:EvaluateComponent,
    children:[
        {
            //Instrumental ADL
            path     : '',
            component: HomeEvaluateComponent
        },
     
        {
            //Instrumental ADL
            path     : 'adl',
            component: AdlComponent
        },
        {
            //Advance care plan
            path     : 'advance',
            component: AdvanceCarePlanComponent
        },
        {
            path     : 'barthel',
            component: BarthelComponent
        },
        {
            //พัฒนาการเด็ก
            path     : 'child-develop',
            component: ChildDevelopmentComponent
        },
        {
            //เยี่ยมทารก
            path     : 'child-visit',
            component: ChildVisitComponent
        },
        {
            //Chronic
            path     : 'cvd-risk',
            component: CvdRiskComponent
        },
        {
            //ผู้สูงอายุ
            path     : 'elderly',
            component: ElderlyComponent
        },
        {
            //โรคติดต่อ
            path     : 'epidem',
            component: EpidemComponent
        },
        {
            //ESAS
            path     : 'esas',
            component: EsasComponent
        },
        {
            //ประเมินภาวะโภชนาการ
            path     : 'evaluate-nutrition',
            component: EvaluateNutritionComponent
        },
        {
            //Modified Ranking Score
            path     : 'modified-ranking',
            component: ModifiedRankingComponent
        },
        {
            //Motor Power
            path     : 'motor-power',
            component: MotorPowerComponent
        },
        {
            path     : 'other',
            component: OtherComponent
        },
        {
            //Pain killer
            path     : 'pain-killer',
            component: PainKillerComponent
        },
        {
            //เยี่ยมมารดา
            path     : 'postnatal',
            component: PostnatalComponent
        },
        {
            //PPS
            path     : 'pps',
            component: PpsComponent
        },
        {
            //จิตเวช>ความก้าวร้าว
            path     : 'psyche',
            component: PsycheComponent
        },
        {
            //ความก้าวร้าว
            path     : 'violently',
            component: ViolentlyComponent
        },
        {
            //คัดกรองภาวะโภชนาการ
            path     : 'screen-nutrition',
            component: ScreenNutritionComponent
        }

    ]
    
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluateRoutes { }
