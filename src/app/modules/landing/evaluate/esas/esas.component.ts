import { Component, ViewEncapsulation,ViewChild,AfterViewInit } from '@angular/core';
import { Router, Navigation } from '@angular/router';
import { EvaluateService } from '../../../../services/evaluate.service';
import { SweetAlertService } from '../../../../shared/sweetalert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Options } from '@angular-slider/ngx-slider';

@Component({
    selector: 'landing-evaluate-esas',
    templateUrl: './esas.component.html',
    styleUrls: ['./esas.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class EsasComponent     {
    userFullname: string;
    validateForm: boolean = false;
    rowsQuestions: any;

    question: any[] = [];
    myNgmodel = { items: [] };
    profilePatient: any;
    question_type_id:number = 8;
    is_old: boolean = false;

    pain_position: string;
    extra_detail: string;
    
    options: Options = {
        showTicksValues: true,
        stepsArray: [
            { value: 0, legend: 'ไม่มี' },
            { value: 1, legend: 'น้อย' },
            { value: 2, legend: 'น้อย' },
            { value: 3, legend: 'น้อย' },
            { value: 4, legend: 'ปานกลาง' },
            { value: 5, legend: 'ปานกลาง' },
            { value: 6, legend: 'ปานกลาง' },
            { value: 7, legend: 'รุนแรง' },
            { value: 8, legend: 'รุนแรง' },
            { value: 9, legend: 'รุนแรง' },
            { value: 10, legend: 'รุนแรง' },
        ]
    };

    disabled = false;
    max = 10;
    min = 0;
    showTicks = true;
    step = 1;
    thumbLabel = true;
    value = 0;
  
    

    /**
     * Constructor
     */
    constructor(
        private evaluateService: EvaluateService,
        private sweetAlertService: SweetAlertService,
        private spinner: NgxSpinnerService,
        private router: Router
    ) {
        this.userFullname = localStorage.getItem('userFullname');
        this.profilePatient = JSON.parse(localStorage.getItem('profileData'));
    }
   
    formatLabel(value: number | null) {
        if (!value) {
          return 0;
        }
        this.value = value;
        console.log(this.value);

        return value;
      }

    ngOnInit(): void {

        this.dataEvaluate();
    }

    async getEvaluate(id: any, question_type_id: any) {

        let info: any = {
            "coc_home_id": id,
            "question_type_id": question_type_id
        }

        try {
            let rs: any = await this.evaluateService.getEvaluate(info);
            // console.log(rs);
            return rs;
        } catch (error) {
            console.log(error);
        }

    }

    async dataEvaluate() {
        let id: any = {
            question_type_id: this.question_type_id
        };
    
        try {
            let rs: any = await this.evaluateService.list(id);
            console.log(rs);
            this.rowsQuestions = rs.question;
            console.log(this.rowsQuestions);

            let result = await this.getEvaluate(this.profilePatient.coc_home_id, this.question_type_id);
            if (result.length > 0) {
                this.is_old = true;
                result.forEach((data: any, index: any) => {
                    var jsonRow: object = {};
                    jsonRow['evauate_id'] = data['evauate_id'];
                    jsonRow['coc_home_id'] = data['coc_home_id'];
                    jsonRow['question_type_id'] = data['question_type_id'];
                    jsonRow['question_id'] = data['question_id'];
                    jsonRow['answer_id'] = data['answer_id'];
                    jsonRow['answer_text'] = data['answer_text'];
                    jsonRow['question_text'] = data['question_text'];
                    jsonRow['note_text'] = data['note_text'];
                    jsonRow['answer_score'] = data['answer_score'];
                    jsonRow['record_by'] = data['record_by'];
                    jsonRow['ngModel'] = '';
                    if(data['question_id'] == 49){
                        this.pain_position = data['note_text'];
                    }
                    if(data['question_id'] == 58){
                        this.extra_detail = data['note_text'];
                    }
                    
                    this.question.push(jsonRow);
                });
                console.log(this.question);
                this.rowsQuestions.forEach((data: any, index: any) => {
                    let index2 = this.question.findIndex(x => x.question_id === data['question_id']);
                    this.rowsQuestions[index].answer_id = this.question[index2].answer_id;
                    this.rowsQuestions[index].answer_text = this.question[index2].answer_text;
                    this.rowsQuestions[index].answer_score = this.question[index2].answer_score;
                    this.rowsQuestions[index].record_by = this.question[index2].record_by;
                    this.rowsQuestions[index].note_text = this.question[index2].note_text;
                });
                console.log(this.rowsQuestions);
            } else {
                let dataQ = rs.question;
                // console.log(this.rowsQuestions);

                dataQ.forEach((data: any, index: any) => {
                    // key['index'] = val + 1;
                    // console.log(data);
                    // var jsonRow: object = data;
                    var jsonRow: object = {};
                    // delete jsonRow['answer'];  
                    jsonRow['coc_home_id'] = this.profilePatient.coc_home_id;
                    jsonRow['question_type_id'] = data['question_type_id'];
                    jsonRow['question_id'] = data['question_id'];
                    jsonRow['answer_id'] = '';
                    jsonRow['answer_text'] = '';
                    jsonRow['note_text'] = '';
                    jsonRow['answer_score'] = '';
                    jsonRow['record_by'] = '';
                    jsonRow['ngModel'] = '';

                    this.question.push(jsonRow);
                });
                console.log(this.question);
            }
        } catch (error: any) {
            console.log(error.error);
        }

    }

    async saveEvaluate() {
        this.spinner.show();
        this.question[0].note_text = this.pain_position;
        this.question[9].note_text = this.extra_detail;
        for (let data of this.question) {
            delete data['question_text'];
        }
        let info = this.question;
        console.log(info);
        let validate = this.validateQuestion();
        // console.log(validate);
        if (validate) {
            let saveResult: boolean;
            for (let inf of info) {
                delete inf['ngModel'];
                try {
                    if(this.is_old){
                        this.question.forEach(async (data: any, index: any) => {
                            // console.log(data);
                            let rs: any = await this.evaluateService.updateEvaluate(data.evauate_id,data);
                        });                    
                    }else{
                        let rs: any = await this.evaluateService.saveEvaluate(inf);
                    // console.log(rs);
                    }
                    saveResult = true;
                } catch (error: any) {
                    saveResult = false;
                    console.log(error.error);
                    this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถบันทึกข้อมูลได้', 'SmartRefer Ubon');
                }
            }
            if (saveResult) {
                // // this.sweetAlertService.success('คำชี้แจง', 'บันทึกข้อมูลเรียบร้อย', 'SmartRefer Ubon');
                this.sweetAlertService.toastSucces('คำชี้แจง', 'บันทึกข้อมูลเรียบร้อย', 'SmartRefer Ubon');
                this.router.navigate(['/evaluate']);
                // this.router.navigate(['/evaluate']);
            } else {
                this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถบันทึกข้อมูลได้', 'SmartRefer Ubon');
            }

        } else {
            this.sweetAlertService.error('คำชี้แจง', 'กรุณากรอกข้อมูลให้ครบ', 'SmartRefer Ubon');

        }

        this.spinner.hide();
    }



    validateQuestion() {
        let validate: boolean = true;

        for (let data of this.question) {
            console.log(data)
            if (data.answer_score == '' && data.question_id != 58) {
                validate = false;
                break;
            }

        }
        return validate;
    }
 
}
