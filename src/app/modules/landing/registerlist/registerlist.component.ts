import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ThaiaddressService } from '../../../services/thaiaddress.service';
import { RegisterService } from '../../../services/register.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { SweetAlertService } from '../../../shared/sweetalert.service';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { Register } from './register';

import moment from "moment";
import * as _ from "lodash";

@Component({
    selector: 'landing-home',
    templateUrl: './registerlist.component.html',
    encapsulation: ViewEncapsulation.None
})
export class RegisterlistComponent {

    ranges: any = {
        Today: [moment(), moment()],
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 7 Days": [moment().subtract(6, "days"), moment()],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "This Month": [moment().startOf("month"), moment().endOf("month")],
        "Last Month": [
            moment().subtract(1, "month").startOf("month"),
            moment().subtract(1, "month").endOf("month"),
        ],
    };

    rowData: any[] = [];
    rowDataFind: any[] = [];
    dataSource: any;
    dataSourecFind: any;
    rowDataFindFullname: any[] = [];
    dataSourecFindFullname: any;
    itemCatagory: any = [];

    // ปฏิทิน
    selectedDateStart: any = { sdate: null, edate: null }; selectedDateEnd: any = { sdate: null, edate: null };
    sdate: any;
    edate: any;
    fullname: any;
    validateForm: boolean = false;
    hcode: any = localStorage.getItem('hcode');
    displayedColumns: string[] = ['pcucodesend', 'urgency', 'ptstatus', 'name', 'address', 'phone', 'dchdate', 'prediag', 'typerefer', 'last_visit', 'action'];
    isSearch: boolean = false;
    searchData: any = [];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    /**
     * Constructor
     */
    constructor(
        private router: Router,
        private registerService: RegisterService,
        private spinner: NgxSpinnerService,
        private sweetAlertService: SweetAlertService,
        private thaiaddressService: ThaiaddressService,

    ) {
        let username = sessionStorage.getItem('username');
        if (!username) {
            this.router.navigate(['/sign-in']);
        }
        sessionStorage.setItem('route', '/registerlist');
    }

    async ngOnInit() {
        await this.select_coc_from_hcode();
    }

    async list_catagory() {
        try {
            await this.registerService.list_catagory().then((res: any) => {
                this.itemCatagory = res;
            });
        } catch (error) {
            console.log(error);
        }
    }

    async select_coc_from_hcode() {
        this.spinner.show();
        await this.list_catagory()
        // console.log(this.itemCatagory);
        if(this.isSearch){
            this.dataSource = this.searchData;
        } else {
            try {
                let info = {
                    "hcode": this.hcode
                }
                await this.registerService.select_coc_from_hcode(info).then((res: any) => {
                    for (let i = 0; i < res.length; i++) {
                        res[i].d_update = new Date(res[i].d_update);
    
                        let clause = res[i].evaluate_cause.split(',');
                        let catagory_name = '';
                        for (let j = 0; j < clause.length; j++) {
                            let catagory: any = this.itemCatagory.find(e => e.catagory_id == clause[j]);
                            if (catagory) {
                                catagory_name += catagory.catagory_name + ',';
                            } else {
                                catagory_name = '';
                            }
                        }
                        res[i].clause = catagory_name.substring(0, catagory_name.length - 1);
                    }
                    this.rowData = res;
                    this.rowDataFind = this.rowData;
                    this.rowDataFindFullname = this.rowData;
                    this.dataSource = new MatTableDataSource<Register>(this.rowData);
                    this.dataSource.paginator = this.paginator;
                    this.spinner.hide();
                });
    
            } catch (error) {
                console.log(error);
                this.spinner.hide();
            }
        }

    }

    async sendhomevisitview(i: any) {
        if (i) {
            localStorage.setItem('profileData', JSON.stringify(i));
            sessionStorage.setItem('itemStorage', JSON.stringify(i))
            this.router.navigate(['/sendhomevisitview']);
        }
    }
    async homevisit(i: any) {
        if (i) {
            sessionStorage.setItem('itemRegister', JSON.stringify(i))
            this.router.navigate(['/homevisit']);
        }
    }
    async new_homevisit(i: any) {
        if (i) {
            sessionStorage.setItem('itemRegister', JSON.stringify(i))
            this.router.navigate(['/homevisit']);
        }
    }
    async evaluate(profileData) {
        localStorage.setItem('profileData', JSON.stringify(profileData));
        this.router.navigate(['/evaluate']);
    }
    async result(i: any) {
        if (i) {
            sessionStorage.setItem('itemRegister', JSON.stringify(i))
            this.router.navigate(['/result']);
        }
    }


    async search() {
        this.fullname = null;
        let start_date = this.sdate;
        let end_date = this.edate;
        let hcode = this.hcode;
        //หาข้อมูลตามวันที่ d_update
        this.dataSource = new MatTableDataSource<Register>(this.rowDataFind);
        this.dataSource.data = this.dataSource.data.filter(e =>
            moment(e.d_update).format('YYYY-MM-DD') >= moment(start_date).format('YYYY-MM-DD') &&
            moment(e.d_update).format('YYYY-MM-DD') <= moment(end_date).format('YYYY-MM-DD')
        );
        this.dataSource.paginator = this.paginator;
        this.searchData = this.dataSource;
        this.isSearch = true;
    }

    async search_fullname() {
        //หาข้อมูลตามวันที่อัพเดทข้อมูล
        // console.log("fullname Show");
        // console.log(this.fullname);
        if (this.fullname) {
            //หาข้อมูลชื่อและนามสกุล
            this.dataSource = new MatTableDataSource<Register>(this.rowDataFindFullname);
            this.dataSource.data = this.dataSource.data.filter(e => _.includes(e.first_name, this.fullname) || _.includes(e.last_name, this.fullname));
            this.dataSource.paginator = this.paginator;
            // console.log("xxx", this.dataSource.data);

        }
        else {
            this.sweetAlertService.error('คำชี้แจง', 'ไม่พบข้อมูล ' + this.fullname + ' ในระบบฐานข้อมูล', 'SmartRefer Ubon');
        }

    }

    async clear() {
        // this.sdate = null;
        // this.edate = null;
        this.fullname = null;
        this.dataSource = [];
        this.select_coc_from_hcode();
        this.isSearch = false;
        this.searchData = [];
    }



    edit(i: any) {
        sessionStorage.setItem('itemStorage', JSON.stringify(i));
        sessionStorage.setItem('route', '/registerlist');
        sessionStorage.setItem('action', 'editregister');
        this.router.navigate(['/registerlist/edit']);
    }

    dateChangeStart(e: any) {
        // console.log("date change start");
        // console.log(e);
        if (e.startDate != null) {
            let syear = e.startDate.$y;
            let smonth = String(e.startDate.$M + 1).padStart(2, "0");
            let sday = String(e.startDate.$D).padStart(2, "0");
            this.sdate = [syear, smonth, sday].join("-");
            //   console.log(this.sdate);
        }
    }
    dateChangeEnd(e: any) {
        // console.log("date change end");
        // console.log(e);
        if (e.startDate != null) {
            let eyear = e.endDate.$y;
            let emonth = String(e.endDate.$M + 1).padStart(2, "0");
            let eday = String(e.endDate.$D).padStart(2, "0");
            this.edate = [eyear, emonth, eday].join("-");
            //   console.log(this.edate);
        }
    }

    async delete(i: any) {
        let confirm = await this.sweetAlertService.confirm('', 'ต้องการลบรายการนี้ ใช่หรือไม่', '');

        if (confirm) {
            let rs: any = await this.registerService.delete(i.coc_register_id);
            this.sweetAlertService.success('คำชี้แจง', 'ลบรายการเรียบร้อยแล้ว', 'SmartRefer Ubon')
            this.select_coc_from_hcode();
        }

    }

}
