import { Route } from '@angular/router';
import { RegisterlistComponent } from 'app/modules/landing/registerlist/registerlist.component';
import { RegisterEditComponent } from 'app/modules/landing/registerlist/register-edit/register-edit.component';

export const registerlistRoutes: Route[] = [
    {
        path     : '',
        component: RegisterlistComponent
    },
    {
        path     : 'edit',
        component: RegisterEditComponent
    }
];
