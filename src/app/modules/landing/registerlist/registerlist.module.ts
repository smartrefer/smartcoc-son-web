import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { RegisterlistComponent } from './registerlist.component';
import { registerlistRoutes } from './registerlist.routing'
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { RegisterEditComponent } from 'app/modules/landing/registerlist/register-edit/register-edit.component';

@NgModule({
    declarations: [
        RegisterlistComponent,
        RegisterEditComponent
    ],
    imports     : [
        RouterModule.forChild(registerlistRoutes),
        MatPaginatorModule,
        SharedModule,
        NgxDaterangepickerMd.forRoot()
    ]
})
export class RegisterlistModule
{
}
