import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { LoginService } from 'app/services/login.service'
import { UserService } from 'app/core/user/user.service';
import { SweetAlertService } from '../../../shared/sweetalert.service';
import { AuthUtils } from 'app/core/auth/auth.utils';

@Component({
    selector     : 'auth-sign-in',
    templateUrl  : './sign-in.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthSignInComponent implements OnInit
{
    @ViewChild('signInNgForm') signInNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    signInForm: UntypedFormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _authService: AuthService,
        private _loginService: LoginService,
        private _formBuilder: UntypedFormBuilder,
        private _router: Router,
        private _userService: UserService,
        private sweetAlertService: SweetAlertService,
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.signInForm = this._formBuilder.group({
            username     : ['', Validators.required],
            password  : ['', Validators.required],
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Sign in
     */
    async signIn()
    {
        // Return if the form is invalid
        if ( this.signInForm.invalid )
        {
            return;
        }

        // Disable the form
        this.signInForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Sign in
        let info: any = this.signInForm.value;
        // console.log(info);

        try {
            //ตัวแปร rs เพื่อเรียกใช้ class _loginService และฟังก์ชั่น login ส่งค่า token กลับมา
            let rs: any = await this._loginService.login(info);
            console.log(rs);
            if(rs.accessToken){
                sessionStorage.setItem('accessToken',rs.accessToken);
                sessionStorage.setItem('fullname',rs.user.name);
                sessionStorage.setItem('username',rs.user.id);
                sessionStorage.setItem('hcode',rs.user.hcode);
                localStorage.setItem('hcode',rs.user.hcode);
                // this._userService.user = rs.user;
                // console.log('AccessToken:',sessionStorage.getItem('accessToken'));
                // console.log('Fullname:',sessionStorage.getItem('fullname'));
                // console.log('Username:',sessionStorage.getItem('username'));
                this.signInForm.enable();
                //OPEN Dashboard
                this._router.navigate(['/home']);

            } else {
                this.signInForm.enable();
                this.sweetAlertService.error('คำชี้แจง', 'Username/Password ไม่ถูกต้อง', 'Smart COC');
            }

        } catch (error: any) {
            console.log(error);
            //messagebox fix
            this.signInForm.enable();
            this.sweetAlertService.error('คำชี้แจง', 'เชื่อมต่อ API ไม่ได้', 'Smart COC');
        }

    }
}
