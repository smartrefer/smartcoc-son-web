import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  accessToken: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.accessToken = sessionStorage.getItem('accessToken');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accessToken
      })
    };
  }

  async select_register(info: object) {
    const _url = `${this.apiUrl}/question/select_register`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async select_coc(info: object) {
    const _url = `${this.apiUrl}/question/select_coc`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async select_coc_patient(hn: any) {
    const _url = `${this.apiUrl}/services_coc/view/${hn}`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async list_catagory() {
    const _url = `${this.apiUrl}/coc_catagory/select`;
    return await this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async list_refer_level() {
    const _url = `${this.apiUrl}/coc_refer_level/select`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async list_patient_status() {
    const _url = `${this.apiUrl}/coc_patient_status/select`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async save(info: object) {
    const _url = `${this.apiUrl}/coc_register/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async update(info: object, id: any) {
    const _url = `${this.apiUrl}/coc_register/update?coc_register_id=${id}`;
    return this.httpClient.put(_url, info, this.httpOptions).toPromise();
  }

  async save_med(info: object) {
    const _url = `${this.apiUrl}/coc_med/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async save_lab(info: object) {
    const _url = `${this.apiUrl}/coc_lab/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async save_xray(info: object) {
    const _url = `${this.apiUrl}/coc_xray/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async save_allergy(info: object) {
    const _url = `${this.apiUrl}/coc_allergy/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async save_diag(info: object) {
    const _url = `${this.apiUrl}/coc_diag/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async coc_med_select(info: object) {
    const _url = `${this.apiUrl}/coc_med/select`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async get_total_send(info: object) {
    const _url = `${this.apiUrl}/question/select_send`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async get_total_visit(info: object) {
    const _url = `${this.apiUrl}/question/select_visit`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }
  async select_regisOne(info: object) {
    const _url = `${this.apiUrl}/question/select_regisOne`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async get_result(info:object) {
    const _url = `${this.apiUrl}/question/get_result`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async get_result_visit(info:object) {
    const _url = `${this.apiUrl}/question/select_get_result`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async get_plan(info:object) {
    const _url = `${this.apiUrl}/question/select_get_plan`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async register_coc(info: string) {
    const data = {
      is_coc : '2'
    };
    const _url = `${this.apiUrl}/question/update_referback?refer_no=`+info;
    return this.httpClient.put(_url, data, this.httpOptions).toPromise();
  }

  async delete(id:any) {
    const _url = `${this.apiUrl}/coc_register/delete?coc_register_id=${id}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async coc_appointment(info:object) {
    const _url = `${this.apiUrl}/buddy/coc_appointment`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }
  async select_coc_from_hcode(info: object) {
    const _url = `${this.apiUrl}/question/select_coc_from_hcode`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async department() {
    const _url = `${this.apiUrl}/services/department`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async list_items() {
    const _url = `${this.apiUrl}/coc_items/select`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }
}