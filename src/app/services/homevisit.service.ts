import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HomevisitService {

  accessToken: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.accessToken = sessionStorage.getItem('accessToken');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accessToken
      })
    };
  }

  async save(info: object) {
    const _url = `${this.apiUrl}/coc_home/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async question2q() {
    const _url = `${this.apiUrl}/question/question`;
    let info = {
      "question_type_id": 1
    }
    return await this.httpClient.post(_url, info).toPromise();
  }
  async question9q() {
    const _url = `${this.apiUrl}/question/question`;
    let info = {
      "question_type_id": 2
    }
    return await this.httpClient.post(_url, info).toPromise();
  }
  async question8q() {
    const _url = `${this.apiUrl}/question/question`;
    let info = {
      "question_type_id": 3
    }
    return await this.httpClient.post(_url, info).toPromise();
  }

  async saveEvaluate(info: object) {
    const _url = `${this.apiUrl}/coc_evaluate/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async getHomeVisit(info: object) {
    const _url = `${this.apiUrl}/coc_home/selectRegister`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }
  
  async getEvaluateScore(info: object) {
    const _url = `${this.apiUrl}/question/select_process_all`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async getEvaluate(info: object) {
    const _url = `${this.apiUrl}/coc_evaluate/select_evaluate`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async getQuestion() {
    const _url = `${this.apiUrl}/coc_question_type/select`;
    return this.httpClient.get(_url).toPromise();
  }

  async getHomeVisitOne(info: object) {
    const _url = `${this.apiUrl}/coc_home/selectOne`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async saveHomeVisit(id,info: object){
    const _url = `${this.apiUrl}/coc_home/update?coc_home_id=`+id;
    return this.httpClient.put(_url, info, this.httpOptions).toPromise();
  }

  async uploadFile(info: object) {
    const _url = `${this.apiUrl}/coc_genogram/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async getGenogram(info: object) {
    const _url = `${this.apiUrl}/coc_genogram/selectOne`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async updateGenogram(id,info: object) {
    const _url = `${this.apiUrl}/coc_genogram/update?coc_genogram_id=`+id;
    return this.httpClient.put(_url, info, this.httpOptions).toPromise();
  }

  async getDiagnosis(info: object) {
    const _url = `${this.apiUrl}/coc_diag/selectRegister`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

}