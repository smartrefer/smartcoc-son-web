import { Route } from '@angular/router';
import { AuthGuard } from 'app/core/auth/guards/auth.guard';
import { NoAuthGuard } from 'app/core/auth/guards/noAuth.guard';
import { LayoutComponent } from 'app/layout/layout.component';
import { InitialDataResolver } from 'app/app.resolvers';

// @formatter:off
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
export const appRoutes: Route[] = [

    // Redirect empty path to '/example'
    {path: '', pathMatch : 'full', redirectTo: 'home'},

    // Redirect signed-in user to the '/example'
    //
    // After the user signs in, the sign-in page will redirect the user to the 'signed-in-redirect'
    // path. Below is another redirection for that path to redirect the user to the desired
    // location. This is a small convenience to keep all main routes together here on this file.
    {path: 'signed-in-redirect', pathMatch : 'full', redirectTo: 'home'},
    
// mark with no sign in ##
    // Auth routes for guests
    {
        path: '',
        canMatch: [NoAuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            {path: 'confirmation-required', loadChildren: () => import('app/modules/auth/confirmation-required/confirmation-required.module').then(m => m.AuthConfirmationRequiredModule)},
            {path: 'forgot-password', loadChildren: () => import('app/modules/auth/forgot-password/forgot-password.module').then(m => m.AuthForgotPasswordModule)},
            {path: 'reset-password', loadChildren: () => import('app/modules/auth/reset-password/reset-password.module').then(m => m.AuthResetPasswordModule)},
            {path: 'sign-in', loadChildren: () => import('app/modules/auth/sign-in/sign-in.module').then(m => m.AuthSignInModule)},
            {path: 'sign-up', loadChildren: () => import('app/modules/auth/sign-up/sign-up.module').then(m => m.AuthSignUpModule)}
        ]
    },

    // Auth routes for authenticated users
    {
        path: '',
        canMatch: [AuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            {path: 'sign-out', loadChildren: () => import('app/modules/auth/sign-out/sign-out.module').then(m => m.AuthSignOutModule)},
            {path: 'unlock-session', loadChildren: () => import('app/modules/auth/unlock-session/unlock-session.module').then(m => m.AuthUnlockSessionModule)}
        ]
    },

    // Landing routes
    {
        path: '',
        // canMatch: [NoAuthGuard],
        component: LayoutComponent,
        resolve: {
            initialData: InitialDataResolver,
        },
        children: [
            {path: 'home', loadChildren: () => import('app/modules/landing/home/home.module').then(m => m.LandingHomeModule)},
            {path: 'register', loadChildren: () => import('app/modules/landing/register/register.module').then(m => m.RegisterModule)},
            {path: 'coc', loadChildren: () => import('app/modules/landing/coc/coc.module').then(m => m.CocModule)},
            {path: 'registerlist', loadChildren: () => import('app/modules/landing/registerlist/registerlist.module').then(m => m.RegisterlistModule)},
            {path: 'sendhomevisit', loadChildren: () => import('app/modules/landing/sendhomevisit/sendhomevisit.module').then(m => m.SendhomevisitModule)},
            {path: 'result', loadChildren: () => import('app/modules/landing/result/result.module').then(m => m.ResultModule)},
            {path: 'evaluate', loadChildren: () => import('app/modules/landing/evaluate/evaluate.module').then(m => m.EvaluateModule)},
        //    {path: 'examples', loadChildren: () => import('app/modules/landing/example-page.component/example-page.component.module').then(m => m.ExamplePageComponentModule)},
            {path: 'sendhomevisitview', loadChildren: () => import('app/modules/landing/sendhomevisit_view/sendhomevisit_view.module').then(m => m.SendhomevisitViewModule)},
            {path: 'homevisit', loadChildren: () => import('app/modules/landing/home-visit/home-visit.module').then(m => m.HomeVisitModule)},
            {path: 'evaluate',loadChildren: () => import('app/modules/landing/evaluate/evaluate.module').then(m => m.EvaluateModule)},
            {path: 'homevisitview', loadChildren: () => import('app/modules/landing/home-visit-view/home-visit-view.module').then(m => m.HomeVisitViewModule)},
        ]
    },

    // Admin routes
    {
        path: '',
        canMatch: [AuthGuard],
        component: LayoutComponent,
        resolve: {
            initialData: InitialDataResolver,
        },
        children: [
            {path: 'exampledd', loadChildren: () => import('app/modules/admin/example/example.module').then(m => m.ExampleModule)},
        ]
    }
];
