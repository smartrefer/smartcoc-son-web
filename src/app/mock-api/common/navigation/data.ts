/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id   : 'home',
        title: 'dashboard',
        tooltip: 'dashboard',
        type : 'basic',
        icon : 'heroicons_outline:view-grid',
        link : '/home'
    },
    {
        id   : 'register',
        title: 'Register',
        tooltip: 'ลงทะเบียนเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:user-add',
        link : '/register'
    },
    {
        id   : 'register_list',
        title: 'Register List',
        tooltip: 'รายการลงทะเบียนเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:user-group',
        link : '/registerlist'
    },
    {
        id   : 'coc',
        title: 'Homecare',
        tooltip: 'บันทึกเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:home',
        link : '/coc'
    }
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id   : 'home',
        title: 'dashboard',
        tooltip: 'dashboard',
        type : 'basic',
        icon : 'heroicons_outline:view-grid',
        link : '/home'
    },
    {
        id   : 'register',
        title: 'Register',
        tooltip: 'ลงทะเบียนเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:user-add',
        link : '/register'
    },
    {
        id   : 'register_list',
        title: 'Register List',
        tooltip: 'รายการลงทะเบียนเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:user-group',
        link : '/registerlist'
    },
    {
        id   : 'coc',
        title: 'Homecare',
        tooltip: 'บันทึกเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:home',
        link : '/coc'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id   : 'home',
        title: 'dashboard',
        tooltip: 'dashboard',
        type : 'basic',
        icon : 'heroicons_outline:view-grid',
        link : '/home'
    },
    {
        id   : 'register',
        title: 'Register',
        tooltip: 'ลงทะเบียนเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:user-add',
        link : '/register'
    },
    {
        id   : 'register_list',
        title: 'Register List',
        tooltip: 'รายการลงทะเบียนเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:user-group',
        link : '/registerlist'
    },
    {
        id   : 'coc',
        title: 'Homecare',
        tooltip: 'บันทึกเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:home',
        link : '/coc'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id   : 'home',
        title: 'dashboard',
        tooltip: 'dashboard',
        type : 'basic',
        icon : 'heroicons_outline:view-grid',
        classes: {
            icon: 'text-violet-700',
        },
        link : '/home'
    },
    {
        id   : 'register',
        title: 'Register',
        tooltip: 'ลงทะเบียนเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:user-add',
        classes: {
            icon: 'text-violet-700',
        },
        link : '/register'
    },
    {
        id   : 'register_list',
        title: 'Register List',
        tooltip: 'รายการลงทะเบียนเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:user-group',
        classes: {
            icon: 'text-violet-700',
        },
        link : '/registerlist'
    },
    {
        id   : 'coc',
        title: 'Homecare',
        tooltip: 'บันทึกเยี่ยมบ้าน',
        type : 'basic',
        icon : 'heroicons_outline:home',
        classes: {
            icon: 'text-violet-700',
        },
        link : '/coc'
    }
];
